<?php

namespace Drupal\xero_example\Form;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\TypedDataManagerInterface;
use Drupal\xero\Form\XeroFormBuilder;
use Drupal\xero\XeroQuery;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Uses Xero form builder to create a form based on a Typed Data definition.
 *
 * @ingroup xero_example
 */
class HelperForm extends FormBase implements ContainerInjectionInterface {

  /**
   * The Xero Query.
   *
   * @var \Drupal\xero\XeroQuery
   */
  protected $query;

  /**
   * The Xero API Form Builder.
   *
   * @var \Drupal\xero\Form\XeroFormBuilder
   */
  protected $formBuilder;

  /**
   * The Typed Data Manager.
   *
   * @var \Drupal\Core\TypedData\TypedDataManagerInterface
   */
  protected $typedDataManager;

  /**
   * Initialization method.
   *
   * @param \Drupal\xero\XeroQuery $query
   *   The xero.query service.
   * @param \Drupal\xero\Form\XeroFormBuilder $formBuilder
   *   The xero.form_builder service.
   * @param \Drupal\Core\TypedData\TypedDataManagerInterface $typedDataManager
   *   The typed_data_manager service.
   */
  public function __construct(XeroQuery $query, XeroFormBuilder $formBuilder, TypedDataManagerInterface $typedDataManager) {
    $this->query = $query;
    $this->formBuilder = $formBuilder;
    $this->typedDataManager = $typedDataManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'xero_add_bank_transaction_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    try {
      $form['employee'] = $this->formBuilder->getElementFor('xero_employee');

      $form['actions'] = ['#type' => 'actions'];
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Add Employee'),
      ];
    }
    catch (PluginException $e) {
      $form['error'] = [
        '#markup' => $e->getMessage(),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Typed Data is a bit odd with list items in that you should append the
    // actual associated array values rather than create the data type. This
    // example shows how to do it the long way.
    $list_definition = $this->typedDataManager->createListDataDefinition('xero_employee');
    $definition = $this->typedDataManager->createDataDefinition('xero_employee');
    /** @var \Drupal\xero\Plugin\DataType\Employee $employee */
    $employee = $this->typedDataManager->create($definition);
    /** @var \Drupal\xero\Plugin\DataType\XeroItemList $employees */
    $employees = $this->typedDataManager->create($list_definition);
    $employees->appendItem([]);

    $values = $form_state->getValue('employee');
    foreach ($values as $key => $value) {
      if ($key !== 'EmployeeID' && $value) {
        $employee->get($key)->setValue($value);
      }
    }

    $employees->set(0, $employee);

    $this->query
      ->setMethod('post')
      ->setType('xero_employee')
      ->setData($employees);

    if ($values['EmployeeID']) {
      $id = substr($values['EmployeeID'], 0, 36);
      $this->query->setId($id);
    }
    /** @var \Drupal\xero\Plugin\DataType\Employee|bool $data */
    $data = $this->query->execute();

    if ($data) {
      $name = $data->get(0)->get('FirstName');
      $this->messenger()->addStatus($this->t('Employee @name saved successfully', ['@name' => $name->getValue()]));
    }
    else {
      $this->messenger()->addError($this->t('An error occurred saving the employee.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('xero.query'),
      $container->get('xero.form_builder'),
      $container->get('typed_data_manager')
    );
  }

}
