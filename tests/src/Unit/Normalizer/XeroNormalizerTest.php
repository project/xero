<?php

namespace Drupal\Tests\xero\Unit\Normalizer;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\TypedData\ListDataDefinition;
use Drupal\serialization\Normalizer\ComplexDataNormalizer;
use Drupal\serialization\Normalizer\TypedDataNormalizer;
use Drupal\Tests\UnitTestCase;
use Drupal\Tests\xero\Traits\XeroGuidTrait;
use Drupal\Tests\xero\Unit\XeroDataTestTrait;
use Drupal\xero\Normalizer\XeroNormalizer;
use Drupal\xero\TypedData\Definition\AccountDefinition;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;
use Symfony\Component\Serializer\Serializer;

/**
 * Test cases for Xero Normalization.
 *
 * @covers \Drupal\xero\Normalizer\XeroNormalizer
 * @group xero
 */
class XeroNormalizerTest extends UnitTestCase {

  use XeroDataTestTrait;
  use XeroGuidTrait;

  /**
   * Data to normalize.
   *
   * @var array
   */
  protected $data;

  /**
   * The xero module normalizer.
   *
   * @var \Drupal\xero\Normalizer\XeroNormalizer
   */
  protected $normalizer;

  /**
   * The core typed data normalizer.
   *
   * @var \Drupal\serialization\Normalizer\TypedDataNormalizer
   */
  protected $typedDataNormalizer;

  /**
   * The core complex data normalizer.
   *
   * @var \Drupal\serialization\Normalizer\ComplexDataNormalizer
   */
  protected $complexDataNormalizer;

  /**
   * The typed data manager mock.
   *
   * @var \\Prophecy\Prophecy\ObjectProphecy
   */
  protected $typedDataManagerProphet;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Account data definition setup.
    $accountDefinition = AccountDefinition::create('xero_account');
    $listDefinition = ListDataDefinition::createFromDataType('xero_account');
    $listDefinition->setItemDefinition($accountDefinition);

    // Typed Data Manager mockery.
    $this->createTypedDataProphet();
    $typedDataManager = $this->typedDataManagerProphet->reveal();

    // Mock the container.
    $container = new ContainerBuilder();
    $container->set('typed_data_manager', $typedDataManager);
    \Drupal::setContainer($container);

    // Setup account data to emulate deserialization.
    $data = [
      'Accounts' => [
        'Account' => [
          [
            'AccountID' => $this->createGuid(),
            'Name' => $this->getRandomGenerator()->word(10),
            'Code' => '200',
            'Type' => 'BANK',
            'UpdatedDateUTC' => '2009-05-14T01:44:26.747',
          ],
        ],
      ],
    ];

    $this->typedDataManagerProphet
      ->createListDataDefinition('xero_account')
      ->willReturn($listDefinition);

    $this->mockTypedData('list', $data['Accounts']['Account'], NULL, $accountDefinition);

    $typedDataManager = $this->typedDataManagerProphet->reveal();
    $container = new ContainerBuilder();
    $container->set('typed_data_manager', $typedDataManager);
    \Drupal::setContainer($container);

    $this->data = $data;

    // Create a normalizer.
    $this->typedDataNormalizer = new TypedDataNormalizer();
    $this->complexDataNormalizer = new ComplexDataNormalizer();
    $this->normalizer = new XeroNormalizer($typedDataManager);
    $this->normalizer->setSerializer(new Serializer([
      $this->complexDataNormalizer,
      $this->normalizer,
      $this->typedDataNormalizer,
    ]));
  }

  /**
   * Assert that exception is thrown for missing plugin id argument.
   */
  public function testMissingPluginId() {
    $this->expectException(UnexpectedValueException::class);
    $this->normalizer->denormalize(NULL, '\Drupal\xero\Plugin\DataType\Account');
  }

  /**
   * Assert that returns a list of accounts.
   *
   * @covers \Drupal\xero\Normalizer\XeroNormalizer::denormalize
   */
  public function testDenormalize() {
    /** @var \Drupal\xero\Plugin\DataType\XeroItemList $items */
    $items = $this->normalizer->denormalize($this->data, '\Drupal\xero\Plugin\DataType\Account', NULL, ['plugin_id' => 'xero_account']);

    $this->assertTrue(is_a($items, '\Drupal\xero\Plugin\DataType\XeroItemList'));
  }

  /**
   * Assert that returns a list of 1 account.
   */
  public function testDenormalizeOne() {
    $data = [
      'Accounts' => [
        'Account' => $this->data['Accounts']['Account'][0],
      ],
    ];

    $items = $this->normalizer->denormalize(
      $data,
      '\Drupal\xero\Plugin\DataType\Account',
      NULL,
      ['plugin_id' => 'xero_account']
    );
    $this->assertTrue(is_a($items, '\Drupal\xero\Plugin\DataType\XeroItemList'));
  }

}
