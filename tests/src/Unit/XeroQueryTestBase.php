<?php

namespace Drupal\Tests\xero\Unit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\serialization\Normalizer\ComplexDataNormalizer;
use Drupal\serialization\Normalizer\TypedDataNormalizer;
use Drupal\Tests\UnitTestCase;
use Drupal\Tests\xero\Traits\XeroGuidTrait;
use Drupal\xero\Normalizer\XeroNormalizer;
use Drupal\xero\XeroQuery;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Component\Serializer\Serializer;

/**
 * Class XeroQueryTestBase provides helper methods for unit tests.
 */
abstract class XeroQueryTestBase extends UnitTestCase {

  use ProphecyTrait;
  use XeroGuidTrait;

  /**
   * The typed data manager.
   *
   * @var \Drupal\Core\TypedData\TypedDataManager
   */
  protected $typedDataManager;

  /**
   * Symfony serializer.
   *
   * @var \Symfony\Component\Serializer\Serializer
   */
  protected $serializer;

  /**
   * A logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The guzzle client.
   *
   * @var \Radcliffe\Xero\XeroClient
   */
  protected $client;

  /**
   * Null cache backend.
   *
   * @var \Drupal\Core\Cache\NullBackend
   */
  protected $cache;

  /**
   * The xero query instance.
   *
   * @var \Drupal\xero\XeroQuery
   */
  protected $query;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Setup Null cache backend.
    $this->cache = $this->createMock('Drupal\Core\Cache\NullBackend');

    // Setup LoggerChannelFactory.
    $logProphet = $this->prophesize('\Psr\Log\LoggerInterface');
    $logProphet->error(Argument::any(), Argument::any());
    $logger = $logProphet->reveal();
    $loggerFactoryProphet = $this->prophesize('\Drupal\Core\Logger\LoggerChannelFactoryInterface');
    $loggerFactoryProphet->addLogger($logger);
    $loggerFactoryProphet->get('xero')->willReturn($logger);
    $this->loggerFactory = $loggerFactoryProphet->reveal();

    // Mock the Typed Data Manager.
    $this->typedDataManager = $this->createMock('Drupal\Core\TypedData\TypedDataManager');

    // Mock XeroClient.
    $this->client = $this->createMock('\Radcliffe\Xero\XeroClient');

    // Setup container.
    $container = new ContainerBuilder();
    $container->set('typed_data_manager', $this->typedDataManager);
    $container->set('serializer', $this->serializer);
    \Drupal::setContainer($container);

    // Setup Serializer component class.
    $this->serializer = new Serializer([
      new ComplexDataNormalizer(),
      new XeroNormalizer($this->typedDataManager),
      new TypedDataNormalizer(),
    ]);

    $this->query = new XeroQuery($this->client, $this->serializer, $this->typedDataManager, $this->loggerFactory, $this->cache);
  }

}
