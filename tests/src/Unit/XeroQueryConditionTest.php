<?php

namespace Drupal\Tests\xero\Unit;

/**
 * Tests xero query conditions.
 *
 * @group xero
 */
class XeroQueryConditionTest extends XeroQueryTestBase {

  /**
   * Asserts exception on bad condition.
   */
  public function testBadCondition() {
    $this->expectException(\InvalidArgumentException::class);
    $this->query->addCondition('Name', 'a', 'garbage');
  }

  /**
   * Asserts exception on bad logical operator.
   */
  public function testBadLogicalOperator() {
    $this->expectException(\InvalidArgumentException::class);
    $this->query->addOperator('NOT');
  }

  /**
   * Assert logical operator.
   */
  public function testLogicalOperator() {
    $this->query->addOperator('OR');
    $conditions = $this->query->getConditions();
    $this->assertEquals('OR', $conditions[0]);
  }

  /**
   * Assert that operators work correctly.
   *
   * @dataProvider operatorProvider
   */
  public function testOperators($operator, $value, $expected) {
    $this->query->addCondition('Name', $value, $operator);
    $conditions = $this->query->getConditions();
    $this->assertEquals(1, count($conditions));
    $this->assertEquals($expected, $conditions[0]);
  }

  /**
   * Assert that Guid operator works correctly.
   */
  public function testGuidOperator() {
    $guid = $this->createGuid();
    $this->query->addCondition('ContactID', $guid, 'guid');
    $conditions = $this->query->getConditions();
    $this->assertEquals('ContactID= Guid("' . $guid . '")', $conditions[0]);
  }

  /**
   * Provide options for testing operators.
   *
   * @return array
   *   An array of conditions.
   */
  public static function operatorProvider() {
    return [
      ['==', 'a', 'Name=="a"'],
      ['==', FALSE, 'Name=="false"'],
      ['!=', 'a', 'Name!="a"'],
      ['StartsWith', 'a', 'Name.StartsWith("a")'],
      ['EndsWith', 'a', 'Name.EndsWith("a")'],
      ['Contains', 'a', 'Name.Contains("a")'],
      ['NULL', '', 'Name==null'],
      ['NOT NULL', '', 'Name!=null'],
    ];
  }

}
