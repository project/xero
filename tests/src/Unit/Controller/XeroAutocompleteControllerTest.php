<?php

namespace Drupal\Tests\xero\Unit\Controller;

use Drupal\Component\Utility\Random;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\ListDataDefinition;
use Drupal\Core\TypedData\Plugin\DataType\ItemList;
use Drupal\Core\TypedData\Plugin\DataType\StringData;
use Drupal\Tests\UnitTestCase;
use Drupal\Tests\xero\Traits\XeroGuidTrait;
use Drupal\xero\Controller\XeroAutocompleteController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Assert that autocomplete works based on mocked XeroQuery output.
 *
 * @group xero
 *
 * @coversDefaultClass \Drupal\xero\Controller\XeroAutocompleteController
 */
class XeroAutocompleteControllerTest extends UnitTestCase {

  use XeroGuidTrait;

  /**
   * The xero data definition used in the test.
   *
   * @var \Drupal\Core\TypedData\DataDefinitionInterface
   */
  protected $definition;

  /**
   * The service container.
   *
   * @var \Drupal\Core\DependencyInjection\ContainerBuilder
   */
  protected $container;

  /**
   * Mocked typed data manager.
   *
   * @var \Drupal\Core\TypedData\TypedDataManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $typedDataManager;

  /**
   * Mocked xero query class instance.
   *
   * @var \Drupal\xero\XeroQuery|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $xeroQuery;

  /**
   * Assert that autocomplete functions.
   *
   * @dataProvider dataProvider
   */
  public function testAutocomplete($plugin_id, $definition_class_name, $type_name, $search_term, $search_guid, $expects) {
    // Create the data definition for this data type.
    $this->definition = $definition_class_name::create($plugin_id);
    $this->definition->setClass($type_name);

    // Create list data definition and item list.
    $listDefinition = ListDataDefinition::create($plugin_id);
    $listDefinition->setClass('\Drupal\Core\TypedData\Plugin\DataType\ItemList');

    $stringDefinition = DataDefinition::create('string');

    // Create item list.
    $itemList = ItemList::createInstance($listDefinition);

    // Create data types.
    $items = [];
    $data = [];
    $dataMap = [];
    if (!empty($expects)) {
      $guid_name = $type_name::$guid_name;
      $label = $type_name::$label;
      foreach ($expects as $num => $values) {
        $item = $type_name::createInstance($this->definition);
        $data[$num] = [$guid_name => $values['value'], $label => $values['label']];
        $item->setValue($data[$num], FALSE);

        if (isset($values['label'])) {
          $expects[$num]['value'] .= ' (' . $values['label'] . ')';
        }
        else {
          $expects[$num]['label'] = $values['value'];
        }
        $items[] = $item;

        // Create data type instances for.
        $data[$num]['valueData'] = new StringData($stringDefinition);
        $data[$num]['valueData']->setValue($values['value']);
        $data[$num]['labelData'] = new StringData($stringDefinition);
        $data[$num]['labelData']->setValue($values['label']);
        $dataMap[] = [$itemList, $num, $item, $item];
        $dataMap[] = [$item, $label, $values['label'], $data[$num]['labelData']];
        $dataMap[] = [$item, $guid_name, $values['value'], $data[$num]['valueData']];
      }
    }

    // Mock typed data manager return.
    $this->typedDataManager->expects($this->any())
      ->method('getDefinition')
      ->with($plugin_id)
      ->willReturn([
        'id' => $plugin_id,
        'definition class' => $definition_class_name,
      ]);
    $this->typedDataManager->expects($this->any())
      ->method('createDataDefinition')
      ->with($plugin_id)
      ->willReturn($this->definition);
    $this->typedDataManager->expects($this->any())
      ->method('createListDataDefinition')
      ->with($plugin_id)
      ->willReturn($listDefinition);
    if (empty($items)) {
      $this->typedDataManager->expects($this->any())
        ->method('getPropertyInstance');
    }
    else {
      // Add as many calls to getPropertyInstance as necessary depending on the
      // number of items that are being returned in the item list.
      $this->typedDataManager->expects($this->any())
        ->method('getPropertyInstance')
        ->willReturnMap($dataMap);
    }

    // Set the list data type.
    $itemList->setValue($items);

    // Mock the query object.
    $this->xeroQuery->expects($this->any())
      ->method('setType')
      ->with($plugin_id)
      ->willReturnSelf();
    $this->xeroQuery->expects($this->any())
      ->method('setMethod')
      ->with('get')
      ->willReturnSelf();
    $this->xeroQuery->expects($this->any())
      ->method('setFormat')
      ->with('pdf')
      ->willReturnSelf();
    $this->xeroQuery->expects($this->any())
      ->method('addCondition')
      ->with('Name', $search_term, 'Contains')
      ->willReturnSelf();
    $this->xeroQuery->expects($this->any())
      ->method('setId')
      ->with($search_guid)
      ->willReturnSelf();
    $this->xeroQuery->expects($this->any())
      ->method('execute')
      ->willReturn($itemList);

    // Instantiate the XeroAutocompleteController.
    $controller = XeroAutocompleteController::create($this->container);
    // Do the request.
    $request = Request::create(
      '/xero/autocomplete/' . $plugin_id . '/' . $search_term,
      'GET',
      ['q' => $search_term]);
    $response = $controller->autocomplete($request, $plugin_id);

    // Assert response content.
    $this->assertEquals(json_encode($expects), $response->getContent());
  }

  /**
   * Provide typed data for Unit tests.
   */
  public static function dataProvider(): array {
    $data = [];
    $randomGenerator = new Random();
    $search_term = $randomGenerator->word(10);
    $search_guid = self::createGuidWithBraces();

    $expects = [
      0 => [
        'value' => self::createGuid(),
        'label' => $search_term . $randomGenerator->word(4),
      ],
      1 => [
        'value' => self::createGuid(),
        'label' => $search_term . $randomGenerator->word(4),
      ],
    ];

    $transaction = [
      0 => [
        'value' => self::createGuid(),
        'label' => NULL,
      ],
    ];

    $data[] = [
      'xero_contact',
      '\Drupal\xero\TypedData\Definition\ContactDefinition',
      '\Drupal\xero\Plugin\DataType\Contact',
      $search_term,
      $search_guid,
      [],
    ];
    $data[] = [
      'xero_contact',
      '\Drupal\xero\TypedData\Definition\ContactDefinition',
      '\Drupal\xero\Plugin\DataType\Contact',
      $search_term,
      $search_guid,
      [0 => $expects[0]],
    ];
    $data[] = [
      'xero_contact',
      '\Drupal\xero\TypedData\Definition\ContactDefinition',
      '\Drupal\xero\Plugin\DataType\Contact',
      $search_term,
      $search_guid,
      $expects,
    ];
    $data[] = [
      'xero_bank_transaction',
      '\Drupal\xero\TypedData\Definition\BankTransactionDefinition',
      '\Drupal\xero\Plugin\DataType\BankTransaction',
      $search_guid,
      $search_guid,
      $transaction,
    ];

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Setup TypedDataManager mock.
    // Typed Data Manager setup.
    $this->typedDataManager = $this->createMock('\Drupal\Core\TypedData\TypedDataManager');

    // Setup Xero Query mock.
    $this->xeroQuery = $this->createMock('\Drupal\xero\XeroQuery');

    // Mock the container.
    $this->container = new ContainerBuilder();
    $this->container->set('typed_data_manager', $this->typedDataManager);
    $this->container->set('xero.query', $this->xeroQuery);
    \Drupal::setContainer($this->container);
  }

}
