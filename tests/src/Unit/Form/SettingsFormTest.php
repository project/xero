<?php

namespace Drupal\Tests\xero\Unit\Form;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Form\FormState;
use Drupal\Tests\Core\Form\FormTestBase;
use Drupal\Tests\xero\Traits\XeroTokenTrait;
use Drupal\xero\Form\SettingsForm;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * Test the Xero configuration form.
 *
 * @coversDefaultClass \Drupal\xero\Form\SettingsForm
 * @group xero
 */
class SettingsFormTest extends FormTestBase {

  use ProphecyTrait;
  use XeroTokenTrait;

  /**
   * The settings form instance to test.
   *
   * @var \Drupal\xero\Form\SettingsForm
   */
  protected $settingsForm;

  /**
   * Consumer id mock.
   *
   * @var string
   */
  protected $consumerId;

  /**
   * Consumer secret mock.
   *
   * @var string
   */
  protected $consumerSecret;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->consumerId = $this->createToken();
    $this->consumerSecret = $this->createToken();

    // Mock config objects with mock objects because prophecy is opinionated.
    $immutableConfig = $this->createMock('\Drupal\Core\Config\ImmutableConfig');
    $immutableConfig->expects($this->any())
      ->method('get')
      ->willReturnMap([
        [
          'oauth.consumer_key' => $this->consumerId,
          'oauth.consumer_secret' => $this->consumerSecret,
        ],
      ]);

    $mutableConfig = $this->createMock('\Drupal\Core\Config\Config');
    $mutableConfig->expects($this->any())
      ->method('set')
      ->willReturnSelf();
    $mutableConfig->expects($this->any())
      ->method('get')
      ->willReturnCallback(fn ($key) => $immutableConfig->get($key));

    // Mock ConfigFactory service.
    $configFactoryProphet = $this->prophesize('\Drupal\Core\Config\ConfigFactoryInterface');
    $configFactoryProphet
      ->getEditable('xero.settings')
      ->willReturn($mutableConfig);
    $configFactoryProphet
      ->get('xero.settings')
      ->willReturn($immutableConfig);

    // Mock Logger Channel Factory service.
    $loggerProphet = $this->prophesize('\Drupal\Core\Logger\LoggerChannelInterface');
    $loggerFactoryProphet = $this->prophesize('\Drupal\Core\Logger\LoggerChannelFactoryInterface');
    $loggerFactoryProphet->get('xero')->willReturn($loggerProphet->reveal());

    $translateProphet = $this->prophesize('\Drupal\Core\StringTranslation\TranslationManager');

    $csrfProphet = $this->prophesize('\Drupal\Core\Access\CsrfTokenGenerator');
    $csrfProphet->get()->willReturn($this->createToken());

    $urlProphet = $this->prophesize('\Drupal\Core\Routing\UrlGeneratorInterface');
    $urlProphet
      ->generateFromRoute(Argument::any(), Argument::any(), Argument::any(), Argument::any())
      ->willReturn('https://example.com/');

    $tempStoreProphet = $this->prophesize('\Drupal\Core\TempStore\PrivateTempStore');
    $tempStoreProphet->set('global', Argument::any());
    $tempStoreFactoryProphet = $this->prophesize('\Drupal\Core\TempStore\PrivateTempStoreFactory');
    $tempStoreFactoryProphet->get('xero.auth')->willReturn($tempStoreProphet->reveal());

    $typedConfigProphet = $this->prophesize('\Drupal\Core\Config\TypedConfigManagerInterface');

    // Mock the container.
    $container = new ContainerBuilder();
    $container->set('logger.factory', $loggerFactoryProphet->reveal());
    $container->set('config.factory', $configFactoryProphet->reveal());
    $container->set('config.typed', $typedConfigProphet->reveal());
    $container->set('string_translation', $translateProphet->reveal());
    $container->set('csrf_token', $csrfProphet->reveal());
    $container->set('url_generator', $urlProphet->reveal());
    $container->set('tempstore.private', $tempStoreFactoryProphet->reveal());
    \Drupal::setContainer($container);
    $this->settingsForm = SettingsForm::create($container);
  }

  /**
   * Asserts that form submit calls config.
   */
  public function testSubmitForm() {
    $messengerProphet = $this->prophesize('\Drupal\Core\Messenger\MessengerInterface');
    $messengerProphet->addStatus(Argument::any())->shouldBeCalled();

    $this->settingsForm->setMessenger($messengerProphet->reveal());

    $form_state = new FormState();
    $form = $this->settingsForm->buildForm([], $form_state);

    $form_state->setValues([
      'oauth' => [
        'consumer_key' => $this->consumerId,
        'consumer_secret' => $this->consumerSecret,
        'redirect_uri' => 'https://example.com/xero/authorize',
      ],
    ]);

    $this->settingsForm->submitForm($form, $form_state);
  }

  /**
   * Asserts that the authorize form submit redirects to Xero.
   */
  public function testAuthorize() {
    $form_state = new FormState();
    $form = $this->settingsForm->buildForm([], $form_state);

    $form_state->setValues([
      'oauth' => [
        'consumer_key' => $this->consumerId,
        'consumer_secret' => $this->consumerSecret,
        'redirect_uri' => 'https://example.com/xero/authorize',
      ],
    ]);

    $this->settingsForm->authorize($form, $form_state);

    $this->assertInstanceOf('\Drupal\Core\Routing\TrustedRedirectResponse', $form_state->getResponse());
  }

}
