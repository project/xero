<?php

namespace Drupal\Tests\xero\Unit\Form;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Tests\UnitTestCase;
use Drupal\xero\Form\XeroFormBuilder;
use Drupal\xero\TypedData\Definition\AddressDefinition;
use Drupal\xero\TypedData\Definition\ContactDefinition;
use Drupal\xero\TypedData\Definition\PhoneDefinition;

/**
 * Tests building form elements from xero types.
 *
 * @group xero
 * @coversDefaultClass \Drupal\xero\Form\XeroFormBuilder
 */
class XeroFormBuilderTest extends UnitTestCase {

  /**
   * An instance of the class to test.
   *
   * @var \Drupal\xero\Form\XeroFormBuilder
   */
  protected $formBuilder;

  /**
   * Xero Contact Definition.
   *
   * @var \Drupal\xero\TypedData\Definition\ContactDefinition
   */
  protected $contactDefinition;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create some data definitions.
    $this->contactDefinition = ContactDefinition::create('xero_contact');
    $addressDefinition = AddressDefinition::create('xero_address');
    $phoneDefinition = PhoneDefinition::create('xero_phone');

    // Mock Typed Data Manager.
    $typedDataManager = $this->createMock('\Drupal\Core\TypedData\TypedDataManager');
    $typedDataManager->expects($this->any())
      ->method('getDefaultConstraints')
      ->willReturn([]);
    $typedDataManager->expects($this->any())
      ->method('createDataDefinition')
      ->willReturnMap([
        ['xero_contact', $this->contactDefinition],
        ['xero_address', $addressDefinition],
        ['xero_phone', $phoneDefinition],
      ]);

    $this->formBuilder = new XeroFormBuilder($typedDataManager);

    $container = new ContainerBuilder();
    $container->set('typed_data_manager', $typedDataManager);
    \Drupal::setContainer($container);
  }

  /**
   * Tests creating element array from xero type.
   */
  public function testGetElementFor(): void {
    // Get the full element for a contact data definition.
    $element = $this->formBuilder->getElementFor('xero_contact');

    // Assert element types.
    $this->assertEquals('textfield', $element['Name']['#type']);
    $this->assertEquals('Name', $element['Name']['#title']);
    $this->assertEquals('checkbox', $element['IsSupplier']['#type']);
    $this->assertEquals('Is supplier?', $element['IsSupplier']['#title']);
    $this->assertEquals('container', $element['Addresses']['#type']);
    $this->assertEquals('select', $element['Addresses'][0]['AddressType']['#type']);
    $this->assertEquals(['POBOX' => 'POBOX', 'STREET' => 'STREET', 'DELIVERY' => 'DELIVERY'], $element['Addresses'][0]['AddressType']['#options']);

    // Assert that read-only property is not added to the element.
    $this->assertArrayNotHasKey('Website', $element);
  }

  /**
   * Tests creating an autocomplete element for a GUID property.
   */
  public function testGetIDElement(): void {
    $element = $this->formBuilder->getElementForDefinition($this->contactDefinition, 'ContactID');

    $this->assertEquals('xero.autocomplete', $element['#autocomplete_route_name']);
    $this->assertEquals(['type' => 'xero_contact'], $element['#autocomplete_route_parameters']);
  }

}
