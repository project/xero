<?php

namespace Drupal\Tests\xero\Unit;

/**
 * Tests Xero query order by.
 *
 * @group xero
 */
class XeroQueryOrderTest extends XeroQueryTestBase {

  /**
   * Assert order by method.
   *
   * @param string $direction
   *   The sort direction to test.
   * @param string $expected
   *   The expected value from the test.
   *
   * @dataProvider directionProvider
   */
  public function testOrderBy($direction, $expected) {
    $this->query->orderBy('Name', $direction);
    $options = $this->query->getOptions();
    $this->assertEquals($expected, $options['query']['order']);
  }

  /**
   * Provide options for testing order by directions.
   *
   * @return string[][]
   *   An array of directions and expected values.
   */
  public static function directionProvider() {
    return [
      ['ASC', 'Name'],
      ['DESC', 'Name DESC'],
    ];
  }

}
