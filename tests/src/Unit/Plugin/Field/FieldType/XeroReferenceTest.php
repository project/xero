<?php

namespace Drupal\Tests\xero\Unit\Plugin\Field\FieldType;

use Drupal\Core\Field\TypedData\FieldItemDataDefinition;
use Drupal\Tests\Core\Field\BaseFieldDefinitionTestBase;
use Drupal\Tests\xero\Traits\XeroGuidTrait;
use Drupal\xero\Plugin\Field\FieldType\XeroReference;

/**
 * Test the constraint system for Xero Guid strings.
 *
 * @coversDefaultClass \Drupal\xero\Plugin\Field\FieldType\XeroReference
 * @group xero
 */
class XeroReferenceTest extends BaseFieldDefinitionTestBase {

  use XeroGuidTrait;

  /**
   * The typed data manager..
   *
   * @var \Drupal\Core\TypedData\TypedDataManagerInterface
   */
  protected $typedDataManager;

  /**
   * The validation constraint manager.
   *
   * @var \Drupal\Core\Validation\ConstraintManager
   */
  protected $validationConstraintManager;

  /**
   * {@inheritdoc}
   */
  protected function getPluginId() {
    return 'xero_reference';
  }

  /**
   * {@inheritdoc}
   */
  protected function getModuleAndPath() {
    return [
      'xero',
      dirname(dirname(dirname(dirname(dirname(dirname(__DIR__)))))),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    require_once realpath($this->root) . '/core/includes/bootstrap.inc';

    // Set the typed data manager service after mocking (again).
    $this->typedDataManager = $this->createMock('\Drupal\Core\TypedData\TypedDataManagerInterface');
    $this->typedDataManager->expects($this->any())
      ->method('getDefaultConstraints')
      ->willReturn([]);
    // Validation constraint manager setup.
    $this->validationConstraintManager = $this->createMock('\Drupal\Core\Validation\ConstraintManager');
    $this->validationConstraintManager
      ->expects($this->any())
      ->method('create')
      ->willReturn([]);
    $this->typedDataManager->expects($this->any())
      ->method('getValidationConstraintManager')
      ->willReturn($this->validationConstraintManager);

    // Set the container.
    $container = \Drupal::getContainer();
    $container->set('typed_data_manager', $this->typedDataManager);
    $container->set('validation.constraint', $this->validationConstraintManager);
    \Drupal::setContainer($container);
  }

  /**
   * Test get columns.
   */
  public function testGetColumns() {
    $columns = [
      'guid' => ['type' => 'varchar', 'length' => 36, 'not null' => TRUE],
      'label' => ['type' => 'varchar', 'length' => 255, 'not null' => FALSE],
      'type' => ['type' => 'varchar', 'length' => 100, 'not null' => TRUE],
    ];
    $this->assertSame($columns, $this->definition->getColumns());
  }

  /**
   * Test main property name.
   */
  public function testMainPropertyName() {
    $this->typedDataManager->expects($this->any())
      ->method('getDefinition')
      ->with('field_item:xero_reference')
      ->willReturn(['class' => '\Drupal\xero\Plugin\Field\FieldType\XeroReference']);
    $this->assertEquals('guid', $this->definition->getMainPropertyName());
  }

  /**
   * Test property definitions.
   */
  public function testPropertyDefinitions() {
    $this->typedDataManager->expects($this->any())
      ->method('getDefinition')
      ->with('field_item:xero_reference', TRUE)
      ->willReturn(['class' => '\Drupal\xero\Plugin\Field\FieldType\XeroReference']);
    $definitions = $this->definition->getPropertyDefinitions();

    $this->assertEquals('GUID', $definitions['guid']->getLabel());
    $this->assertEquals('Label', $definitions['label']->getLabel());
    $this->assertEquals('Type', $definitions['type']->getLabel());
    $constraint = $definitions['type']->getConstraint('XeroChoiceConstraint');
    $this->assertTrue(isset($constraint));
  }

  /**
   * Test getters and setters.
   */
  public function testProperties() {
    $guid = $this->createGuid();
    $guid_with_braces = '{' . $guid . '}';

    $this->typedDataManager->expects($this->any())
      ->method('getDefinition')
      ->with('field_item:xero_reference', TRUE)
      ->willReturn(['class' => '\Drupal\xero\Plugin\Field\FieldType\XeroReference']);

    $fieldItemDefinition = FieldItemDataDefinition::create($this->definition);
    $type = new XeroReference($fieldItemDefinition, 'xero_reference');

    // @todo this probably makes this test have very little value?
    $this->typedDataManager->expects($this->any())
      ->method('getPropertyInstance')
      ->with($type, 'guid')
      ->willReturn($guid);

    $this->assertTrue($type->isEmpty());

    $type->set('guid', 0);
    $this->assertTrue($type->isEmpty());

    $type->set('guid', '{}');
    $this->assertFalse($type->isEmpty());

    $type->set('guid', $guid_with_braces);
    $type->preSave();
    $this->assertEquals($guid, $type->get('guid'));
  }

}
