<?php

namespace Drupal\Tests\xero\Unit\Plugin\Field\FieldWidget;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Field\TypedData\FieldItemDataDefinition;
use Drupal\Tests\UnitTestCase;
use Drupal\xero\Plugin\Field\FieldType\XeroReference;

/**
 * Provides a base test class for testing field widgets.
 */
abstract class WidgetTestBase extends UnitTestCase {

  /**
   * The widget to test.
   *
   * @var \Drupal\Core\Field\WidgetInterface
   */
  protected $widget;

  /**
   * The Drupal field definition.
   *
   * @var \Drupal\Core\Field\BaseFieldDefinition
   */
  protected $fieldDefinition;

  /**
   * The xero reference field.
   *
   * @var \Drupal\xero\Plugin\Field\FieldType\XeroReference
   */
  protected $fieldItem;

  /**
   * The xero reference field item list.
   *
   * @var \Drupal\Core\Field\FieldItemList
   */
  protected $fieldItemList;

  /**
   * The typed data manager.
   *
   * @var \Drupal\Core\TypedData\TypedDataManagerInterface
   */
  protected $typedDataManager;

  /**
   * The field type plugin manager.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  protected $pluginManager;

  /**
   * The plugin id of the widget.
   *
   * @return string
   *   The plugin id of the widget.
   */
  abstract protected function getPluginId();

  /**
   * The plugin class of the widget.
   *
   * @return string
   *   The plugin class of the widget.
   */
  abstract protected function getPluginClass();

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // DrupalWTF: t()
    require_once realpath($this->root . '/core/includes/bootstrap.inc');

    $container = new ContainerBuilder();

    // Mock Typed Data Manager.
    $this->typedDataManager = $this->createMock('\Drupal\Core\TypedData\TypedDataManagerInterface');
    $this->typedDataManager->expects($this->any())
      ->method('getDefaultConstraints')
      ->willReturn([]);
    $this->typedDataManager->expects($this->any())
      ->method('getDefinition')
      ->willReturnMap($this->getDefinitionMap());

    // Mock Field Type Plugin Manager.
    $this->pluginManager = $this->createMock('\Drupal\Core\Field\FieldTypePluginManagerInterface');
    $this->pluginManager->expects($this->any())
      ->method('getDefaultStorageSettings')
      ->with('xero_reference')
      ->willReturn([]);
    $this->pluginManager->expects($this->any())
      ->method('getDefaultFieldSettings')
      ->with('xero_reference')
      ->willReturn([]);

    // Validation constraint manager setup.
    $validation_constraint_manager = $this->createMock('\Drupal\Core\Validation\ConstraintManager');
    $validation_constraint_manager->expects($this->any())
      ->method('create')
      ->willReturn([]);
    $this->typedDataManager->expects($this->any())
      ->method('getValidationConstraintManager')
      ->willReturn($validation_constraint_manager);

    // Set the container again to get rid of stupid base class stuff.
    $container->set('typed_data_manager', $this->typedDataManager);
    $container->set('plugin.manager.field.field_type', $this->pluginManager);
    $container->set('string_translation', $this->getStringTranslationStub());
    \Drupal::setContainer($container);

    // Field definition.
    $this->fieldDefinition = BaseFieldDefinition::create('xero_reference');
    $fieldItemDefinition = FieldItemDataDefinition::create($this->fieldDefinition);

    // Formatter configuration.
    $plugin_definition = [
      'class' => $this->getPluginClass(),
    ];
    $configuration = [
      'field_definition' => $this->fieldDefinition,
      'settings' => [],
      'label' => $this->getRandomGenerator()->word(10),
      'view_mode' => 'default',
      'third_party_settings' => [],
    ];

    $class = $this->getPluginClass();

    if (in_array('Drupal\Core\Plugin\ContainerFactoryPluginInterface', class_implements($class))) {
      $this->widget = $class::create($container, $configuration, $this->getPluginId(), $plugin_definition);
    }
    else {
      $this->widget = new $class($this->getPluginId(), $plugin_definition, $this->fieldDefinition, $configuration['settings'], $configuration['third_party_settings']);
    }

    $this->fieldItemList = new FieldItemList($fieldItemDefinition);
    $this->fieldItem = new XeroReference($fieldItemDefinition);

    $this->fieldDefinition->setSetting('xero_type', ['xero_employee' => 'xero_employee']);
  }

  /**
   * Get definition map for getDefinition call moking.
   *
   * @return array<int, mixed>[]
   *   An array of argument and return value for getDefinition().
   */
  protected function getDefinitionMap() {
    return [
      ['field_item:xero_reference', TRUE, ['class' => '\Drupal\xero\Plugin\Field\FieldType\XeroReference']],
      ['xero_account', TRUE, ['label' => 'Xero Account', 'class' => '\Drupal\xero\Plugin\DataType\Account']],
      [
        'xero_bank_transaction',
        TRUE,
        [
          'label' => 'Xero Bank Transaction',
          'class' => '\Drupal\xero\Plugin\DataType\BankTransaction',
        ],
      ],
      ['xero_contact', TRUE, ['label' => 'Xero Contact', 'class' => '\Drupal\xero\Plugin\DataType\Contact']],
      ['xero_customer', TRUE, ['label' => 'Xero Customer', 'class' => '\Drupal\xero\Plugin\DataType\Customer']],
      ['xero_credit_note', TRUE, ['label' => 'Xero Credit Note', 'class' => '\Drupal\xero\Plugin\DataType\CreditNote']],
      ['xero_employee', TRUE, ['label' => 'Xero Employee', 'class' => '\Drupal\xero\Plugin\DataType\Employee']],
      ['xero_expense', TRUE, ['label' => 'Xero Expense', 'class' => '\Drupal\xero\Plugin\DataType\Expense']],
      ['xero_invoice', TRUE, ['label' => 'Xero Invoice', 'class' => '\Drupal\xero\Plugin\DataType\Invoice']],
      ['xero_journal', TRUE, ['label' => 'Xero Journal', 'class' => '\Drupal\xero\Plugin\DataType\Journal']],
      ['xero_payment', TRUE, ['label' => 'Xero Payment', 'class' => '\Drupal\xero\Plugin\DataType\Payment']],
      ['xero_receipt', TRUE, ['label' => 'Xero Receipt', 'class' => '\Drupal\xero\Plugin\DataType\Receipt']],
      ['xero_user', TRUE, ['label' => 'Xero User', 'class' => '\Drupal\xero\Plugin\DataType\User']],
    ];
  }

}
