<?php

namespace Drupal\Tests\xero\Unit\Plugin\Field\FieldFormatter;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Field\TypedData\FieldItemDataDefinition;
use Drupal\Core\Form\FormState;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\Tests\xero\Traits\XeroGuidTrait;
use Drupal\xero\Plugin\Field\FieldFormatter\XeroReferenceFormatter;
use Drupal\xero\Plugin\Field\FieldType\XeroReference;

/**
 * Test the formatter plugin.
 *
 * This does not use BaseFieldDefinitionTestBase because that class is terrible,
 * and is full of DrupalWTF crap such as not setting useful things as properties
 * such as typed data manager.
 *
 * @coversDefaultClass \Drupal\xero\Plugin\Field\FieldFormatter\XeroReferenceFormatter
 * @group xero
 */
class XeroReferenceFormatterTest extends UnitTestCase {

  use XeroGuidTrait;

  /**
   * The typed data manager.
   *
   * @var \Drupal\Core\TypedData\TypedDataManager|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $typedDataManager;

  /**
   * The field type plugin manager.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManager|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $pluginManager;

  /**
   * The formatter to test.
   *
   * @var \Drupal\xero\Plugin\Field\FieldFormatter\XeroReferenceFormatter
   */
  protected $formatter;

  /**
   * The xero reference field definition.
   *
   * @var \Drupal\Core\Field\FieldDefinitionInterface
   */
  protected $fieldDefinition;

  /**
   * The xero reference field.
   *
   * @var \Drupal\xero\Plugin\Field\FieldType\XeroReference
   */
  protected $fieldItem;

  /**
   * The xero reference field item list.
   *
   * @var \Drupal\Core\Field\FieldItemList
   */
  protected $fieldItemList;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // DrupalWTF. t().
    require_once realpath($this->root . '/core/includes/bootstrap.inc');

    $container = new ContainerBuilder();

    // Mock Typed Data Manager.
    $this->typedDataManager = $this->createMock('\Drupal\Core\TypedData\TypedDataManager');
    $this->typedDataManager->expects($this->any())
      ->method('getDefaultConstraints')
      ->willReturn([]);
    $this->typedDataManager->expects($this->any())
      ->method('getDefinition')
      ->with($this->anything())
      ->willReturnCallback(function ($plugin_id, $exception_on_invalid = TRUE) {
        if ($plugin_id === 'field_item:xero_reference') {
          return ['class' => '\Drupal\xero\Plugin\Field\FieldType\XeroReference'];
        }
        if ($plugin_id === 'xero_employee') {
          return ['class' => '\Drupal\xero\Plugin\DataType\Employee'];
        }
        return NULL;
      });

    // Mock Field Type Plugin Manager.
    $this->pluginManager = $this->createMock('\Drupal\Core\Field\FieldTypePluginManager');
    $this->pluginManager->expects($this->any())
      ->method('getDefaultStorageSettings')
      ->with('xero_reference')
      ->willReturn([]);
    $this->pluginManager->expects($this->any())
      ->method('getDefaultFieldSettings')
      ->with('xero_reference')
      ->willReturn([]);

    // Validation constraint manager setup.
    $validation_constraint_manager = $this->createMock('\Drupal\Core\Validation\ConstraintManager');
    $validation_constraint_manager->expects($this->any())
      ->method('create')
      ->willReturn([]);
    $this->typedDataManager->expects($this->any())
      ->method('getValidationConstraintManager')
      ->willReturn($validation_constraint_manager);

    // Set the container again to get rid of stupid base class stuff.
    $container->set('typed_data_manager', $this->typedDataManager);
    $container->set('plugin.manager.field.field_type', $this->pluginManager);
    $container->set('string_translation', $this->getStringTranslationStub());
    \Drupal::setContainer($container);

    // Field definition.
    $this->fieldDefinition = BaseFieldDefinition::create('xero_reference');
    $fieldItemDefinition = FieldItemDataDefinition::create($this->fieldDefinition);

    // Formatter configuration.
    $plugin_definition = [
      'class' => '\Drupal\xero\Plugin\Field\FieldFormatter\XeroReferenceFormatter',
    ];
    $configuration = [
      'field_definition' => $this->fieldDefinition,
      'settings' => [],
      'label' => $this->getRandomGenerator()->word(10),
      'view_mode' => 'default',
      'third_party_settings' => [],
    ];

    $this->formatter = XeroReferenceFormatter::create($container, $configuration, 'xero_reference', $plugin_definition);
    $this->fieldItemList = new FieldItemList($fieldItemDefinition);
    $this->fieldItem = new XeroReference($fieldItemDefinition);
  }

  /**
   * Test Formatter class.
   */
  public function testFormatterClass() {
    $values = [
      'guid' => $this->createGuidWithBraces(),
      'type' => 'xero_employee',
      'label' => $this->getRandomGenerator()->word(15),
    ];

    $this->fieldItem->setValue($values, FALSE);

    $this->typedDataManager->expects($this->any())
      ->method('getPropertyInstance')
      ->with($this->fieldItemList, 0, $values)
      ->willReturn($this->fieldItem);
    $this->pluginManager->expects($this->any())
      ->method('createFieldItem')
      ->with($this->fieldItemList, 0, $values)
      ->willReturn($this->fieldItem);

    $this->fieldItemList->setValue([0 => $values]);

    $render = $this->formatter->viewElements($this->fieldItemList, LanguageInterface::LANGCODE_NOT_SPECIFIED);

    $this->assertEquals(1, count($render));
    $this->assertTrue(is_a($render[0]['#item'], 'Drupal\xero\Plugin\Field\FieldType\XeroReference'));
  }

  /**
   * Test Formatter Settings.
   */
  public function testFormatterSettings() {
    $form = [];
    $formState = new FormState();

    $settingsSummary = $this->formatter->settingsSummary();
    $this->assertEquals(3, count($settingsSummary));
    $this->assertEquals('Guid: Visible', $settingsSummary[0]);
    $this->assertEquals('Type: Visible', $settingsSummary[1]);
    $this->assertEquals('Label: Visible', $settingsSummary[2]);

    $this->formatter->setSetting('display', []);
    $settingsSummary = $this->formatter->settingsSummary();

    $this->assertEquals('Guid: Hidden', $settingsSummary[0]);
    $this->assertEquals('Type: Hidden', $settingsSummary[1]);
    $this->assertEquals('Label: Hidden', $settingsSummary[2]);

    $settingsForm = $this->formatter->settingsForm($form, $formState);
    $this->assertEquals([], $settingsForm['display']['#default_value']);
  }

}
