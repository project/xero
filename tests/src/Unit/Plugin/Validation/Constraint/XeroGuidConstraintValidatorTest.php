<?php

namespace Drupal\Tests\xero\Unit\Plugin\Validation\Constraint;

use Drupal\Tests\UnitTestCase;
use Drupal\Tests\xero\Traits\XeroGuidTrait;
use Drupal\xero\Plugin\Validation\Constraint\XeroGuidConstraint;
use Drupal\xero\Plugin\Validation\Constraint\XeroGuidConstraintValidator;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Component\Validator\Context\ExecutionContext;

/**
 * Test the constraint system for Xero Guid strings.
 *
 * @coversDefaultClass \Drupal\xero\Plugin\Validation\Constraint\XeroGuidConstraintValidator
 * @group xero
 */
class XeroGuidConstraintValidatorTest extends UnitTestCase {

  use ProphecyTrait;
  use XeroGuidTrait;

  /**
   * The constraint validator.
   *
   * @var \Drupal\xero\Plugin\Validation\Constraint\XeroGuidConstraintValidator
   */
  protected $validator;

  /**
   * The validator context.
   *
   * @var \Symfony\Component\Validator\Context\ExecutionContext
   */
  protected $context;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $translatorProphet = $this->prophesize('\Symfony\Contracts\Translation\TranslatorInterface');
    // $this->translator->trans($message, $parameters, $this->translationDomain)
    $translatorProphet->trans('This value should be a globally-unique identifier.', Argument::any(), Argument::any())
      ->willReturn('This value should be a globally-unique identifier.');

    // Symfony validator and context setup from AbstractConstraintValidatorTest.
    $translator = $translatorProphet->reveal();
    $contextualValidator = $this->createMock('Symfony\Component\Validator\Validator\ContextualValidatorInterface');

    $this->validator = $this->createMock('Symfony\Component\Validator\Validator\ValidatorInterface');

    $this->context = new ExecutionContext($this->validator, 'root', $translator);
    $this->context->setGroup('Xero');
    $this->context->setNode('InvalidValue', NULL, NULL, 'property.path');
    $this->context->setConstraint(new XeroGuidConstraint());

    $this->validator->expects($this->any())
      ->method('inContext')
      ->with($this->context)
      ->willReturn($contextualValidator);

    $this->validator = new XeroGuidConstraintValidator();
    $this->validator->initialize($this->context);
  }

  /**
   * Assert valid GUID values.
   */
  public function testValid() {
    $value = $this->createGuidWithBraces();
    $this->validator->validate($value, new XeroGuidConstraint());
    $this->assertNoViolation();

    $value = $this->createGuid();
    $this->validator->validate($value, new XeroGuidConstraint());
    $this->assertNoViolation();
  }

  /**
   * Assert null value.
   */
  public function testAllowNull() {
    $value = NULL;
    $this->validator->validate($value, new XeroGuidConstraint());
    $this->assertNoViolation();
  }

  /**
   * Assert bad value.
   */
  public function testNotValid() {
    $value = $this->getRandomgenerator()->string(100);
    $constraint = new XeroGuidConstraint();
    $this->validator->validate($value, $constraint);

    $this->assertCount(1, $this->context->getViolations());
    $violation = $this->context->getViolations()->get(0);
    $this->assertEquals('This value should be a globally-unique identifier.', $violation->getMessageTemplate());
  }

  /**
   * Asserts that no constraint violation has occurred.
   *
   * This code is ripped from
   * \Symfony\Component\Validator\Tests\Constraints\AbstractConstraintValidatorTest
   * because it cannot be extended because of Drupal.
   */
  protected function assertNoViolation() {
    $this->assertCount(0, $this->context->getViolations());
  }

}
