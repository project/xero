<?php

namespace Drupal\Tests\xero\Unit\Plugin\DataType;

use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\Plugin\DataType\StringData;

/**
 * Assert setting and getting Phone properties.
 *
 * @coversDefaultClass \Drupal\xero\Plugin\DataType\Phone
 * @group xero
 */
class PhoneTest extends TestBase {

  const XERO_TYPE = 'xero_phone';
  const XERO_TYPE_CLASS = '\Drupal\xero\Plugin\DataType\Phone';
  const XERO_DEFINITION_CLASS = '\Drupal\xero\TypedData\Definition\PhoneDefinition';

  /**
   * Phone data type.
   *
   * @var \Drupal\xero\Plugin\DataType\Phone
   */
  protected $phone;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Create data type.
    $type_class = self::XERO_TYPE_CLASS;
    $this->phone = new $type_class($this->dataDefinition, self::XERO_TYPE);
  }

  /**
   * Test getPropertyDefinitions.
   */
  public function testPropertyDefinitions() {
    $properties = $this->phone->getProperties();

    $this->assertArrayHasKey('PhoneType', $properties);
    $this->assertArrayHasKey('PhoneNumber', $properties);
    $this->assertArrayHasKey('PhoneAreaCode', $properties);
    $this->assertArrayHasKey('PhoneCountryCode', $properties);
  }

  /**
   * Test getPhone method.
   */
  public function testGetPhoneNumber() {
    $string_def = DataDefinition::create('string');
    $country = new StringData($string_def);
    $area = new StringData($string_def);
    $number = new StringData($string_def);

    $this->typedDataManager->expects($this->any())
      ->method('getPropertyInstance')
      ->with($this->phone, $this->callback(fn ($subject) =>
        in_array($subject, ['PhoneCountryCode', 'PhoneAreaCode', 'PhoneNumber'])
      ))
      ->willReturnCallback(function ($object, $property_name, $value = NULL) use ($country, $area, $number) {
        if ($property_name === 'PhoneCountryCode') {
          return $country;
        }
        if ($property_name === 'PhoneAreaCode') {
          return $area;
        }
        if ($property_name === 'PhoneNumber') {
          return $number;
        }
        return NULL;
      });

    $this->phone->set('PhoneCountryCode', '01');
    $this->phone->set('PhoneAreaCode', '805');
    $this->phone->set('PhoneNumber', '255-8542');

    $this->assertEquals('01-805-255-8542', $this->phone->getPhone());
  }

}
