<?php

namespace Drupal\Tests\xero\Unit;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\xero\XeroTokenManager;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prediction\CallPrediction;
use Prophecy\Prophet;

/**
 * Test the Xero token manager.
 *
 * @coversDefaultClass \Drupal\xero\XeroTokenManager
 * @group xero
 */
class XeroTokenManagerTest extends UnitTestCase {

  use ProphecyTrait;

  /**
   * Configuration factory mock.
   *
   * @var object
   */
  protected $configFactoryProphet;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $config = $this->createMock('\Drupal\Core\Config\Config');
    $config->expects($this->any())
      ->method('set')
      ->willReturnSelf();

    $this->configFactoryProphet = $this->prophesize('\Drupal\Core\Config\ConfigFactoryInterface');
    $this->configFactoryProphet->getEditable('xero.settings')->willReturn($config);
  }

  /**
   * Asserts that the setToken method changes based on the arguments.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The account argument for the set method.
   * @param array $organizations
   *   The organizations argument for the set method.
   *
   * @dataProvider setTokenProvider
   */
  public function testSetToken(AccountProxyInterface $account = NULL, array $organizations = []) {
    $userDataProphet = $this->prophesize('\Drupal\user\UserDataInterface');
    $keyValueProphet = $this->prophesize('\Drupal\Core\KeyValueStore\KeyValueStoreInterface');

    $config = $this->createMock('\Drupal\Core\Config\Config');

    if ($account !== NULL) {
      $userDataProphet
        ->set('xero', Argument::any(), 'access_token', 'test')
        ->should(new CallPrediction());
      $userDataProphet
        ->set('xero', Argument::any(), 'refresh_token', 'test')
        ->should(new CallPrediction());
      $userDataProphet
        ->set('xero', Argument::any(), 'expires', 0)
        ->should(new CallPrediction());
      if (!empty($organizations)) {
        $userDataProphet
          ->set('xero', Argument::any(), 'organizations', Argument::any())
          ->should(new CallPrediction());
      }
      $config->expects($this->never())
        ->method('set');
    }
    else {
      $keyValueProphet
        ->setMultiple([
          'access_token' => 'test',
          'refresh_token' => 'test',
          'expires' => 0,
        ])
        ->should(new CallPrediction());
      if (!empty($organizations)) {
        $config->expects($this->once())
          ->method('set')
          ->willReturnSelf();
      }
      else {
        $config->expects($this->never())
          ->method('set');
      }
    }

    $configFactoryProphet = $this->prophesize('\Drupal\Core\Config\ConfigFactoryInterface');
    $configFactoryProphet->getEditable('xero.settings')->willReturn($config);

    $keyFactoryProphet = $this->prophesize('\Drupal\Core\KeyValueStore\KeyValueFactoryInterface');
    $keyFactoryProphet->get('xero')->willReturn($keyValueProphet->reveal());

    $tokenManager = new XeroTokenManager($userDataProphet->reveal(), $keyFactoryProphet->reveal(), $configFactoryProphet->reveal());

    $token = $this->prophesize('\League\OAuth2\Client\Token\AccessTokenInterface');
    $token->getToken()->willReturn('test');
    $token->getRefreshToken()->willReturn('test');
    $token->getExpires()->willReturn(0);

    $tokenManager->setToken($token->reveal(), $account, $organizations);
  }

  /**
   * Asserts that the getToken method changes based on the argument.
   *
   * @param string|bool $expected
   *   The expected instance of the return value.
   * @param bool $global
   *   The global parameter.
   * @param bool $hasPermission
   *   Whether the user has permission.
   *
   * @dataProvider getTokenProvider
   */
  public function testGetToken($expected, $global, $hasPermission) {
    $options = [
      'access_token' => 'test',
      'refresh_token' => 'test',
      'expires' => time() + 3600,
    ];

    $userProphet = $this->prophesize('\Drupal\Core\Session\AccountProxyInterface');
    $userProphet->id()->willReturn(1);
    $userProphet->isAnonymous()->willReturn(FALSE);
    $userProphet->hasPermission('access xero')->willReturn($hasPermission);

    $userDataProphet = $this->prophesize('\Drupal\user\UserDataInterface');
    $keyValueProphet = $this->prophesize('\Drupal\Core\KeyValueStore\KeyValueStoreInterface');

    if ($global) {
      $userProphet
        ->hasPermission('administer site configuration')
        ->willReturn(TRUE);
      $keyValueProphet
        ->getMultiple(['access_token', 'refresh_token', 'expires'])
        ->willReturn($options);
    }
    else {
      $userProphet
        ->hasPermission('administer site configuration')
        ->willReturn(FALSE);
      $userDataProphet
        ->get('xero', 1, 'access_token')
        ->willReturn($options['access_token']);
      $userDataProphet
        ->get('xero', 1, 'refresh_token')
        ->willReturn($options['refresh_token']);
      $userDataProphet
        ->get('xero', 1, 'expires')
        ->willReturn($options['expires']);
    }

    $keyFactoryProphet = $this->prophesize('\Drupal\Core\KeyValueStore\KeyValueFactoryInterface');
    $keyFactoryProphet->get('xero')->willReturn($keyValueProphet->reveal());
    $tokenManager = new XeroTokenManager($userDataProphet->reveal(), $keyFactoryProphet->reveal(), $this->configFactoryProphet->reveal());

    $token = $tokenManager->getToken($userProphet->reveal(), $global);

    if ($expected) {
      $this->assertInstanceOf($expected, $token);
    }
    else {
      $this->assertFalse($token);
    }
  }

  /**
   * Asserts that the user has an access token.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   A user account.
   * @param string $token
   *   The token or NULL.
   * @param bool $expected
   *   The expected value to match.
   *
   * @dataProvider hasUserTokenProvider
   */
  public function testHasUserToken(AccountProxyInterface $account, $token = NULL, $expected = FALSE) {
    $keyValueProphet = $this->prophesize('\Drupal\Core\KeyValueStore\KeyValueStoreInterface');
    $keyFactoryProphet = $this->prophesize('\Drupal\Core\KeyValueStore\KeyValueFactoryInterface');
    $keyFactoryProphet->get('xero')->willReturn($keyValueProphet->reveal());

    $userDataProphet = $this->prophesize('\Drupal\user\UserDataInterface');
    $userDataProphet
      ->get('xero', $account->id(), 'access_token')
      ->willReturn($token);

    $tokenManager = new XeroTokenManager($userDataProphet->reveal(), $keyFactoryProphet->reveal(), $this->configFactoryProphet->reveal());

    $this->assertEquals($expected, $tokenManager->hasUserToken($account));
  }

  /**
   * Provides test arguments for the setToken method.
   *
   * @return array
   *   An array of test arguments.
   */
  public static function setTokenProvider() {
    $prophet = new Prophet();
    $account = $prophet->prophesize('\Drupal\Core\Session\AccountProxyInterface');
    $account->id()->willReturn(1);
    $account->isAnonymous()->willReturn(FALSE);
    return [
      'sets the global token but not organization' => [NULL, []],
      'sets the global token and organization' => [NULL, [['tenantId' => 'testorganization']]],
      'sets a user token but not organization' => [$account->reveal(), []],
      'sets a user token and organization' => [$account->reveal(), [['tenantId' => 'testorganization']]],
    ];
  }

  /**
   * Provides test arguments for the hasUserToken method.
   *
   * @return array
   *   An array of test arguments.
   */
  public static function hasUserTokenProvider() {
    $prophet = new Prophet();
    $account = $prophet->prophesize('\Drupal\Core\Session\AccountProxyInterface');
    $account->id()->willReturn(1);
    $account->isAnonymous()->willReturn(FALSE);
    return [
      'has an access token' => [$account->reveal(), 'test', TRUE],
      'has no access token' => [$account->reveal(), NULL, FALSE],
    ];
  }

  /**
   * Provide test arguments for the getToken method.
   *
   * @return array
   *   An array of test arguments.
   */
  public static function getTokenProvider() {
    return [
      'returns token when global' => [
        '\League\OAuth2\Client\Token\AccessTokenInterface',
        TRUE,
        TRUE,
      ],
      'returns FALSE when global and no permission' => [FALSE, TRUE, FALSE],
      'returns token for user' => [
        '\League\OAuth2\Client\Token\AccessTokenInterface',
        FALSE,
        TRUE,
      ],
      'returns FALSE when user has no permission' => [FALSE, FALSE, FALSE],
    ];
  }

}
