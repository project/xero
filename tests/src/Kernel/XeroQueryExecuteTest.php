<?php

namespace Drupal\Tests\xero\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\xero\Traits\XeroGuidTrait;
use Drupal\Tests\xero\Traits\XeroResponseTrait;
use Drupal\xero\XeroQuery;

/**
 * Tests getting items by guid in XeroQuery.
 *
 * @group xero
 */
class XeroQueryExecuteTest extends KernelTestBase {

  use XeroGuidTrait;
  use XeroResponseTrait;

  /**
   * Xero Query.
   *
   * @var \Drupal\xero\XeroQuery
   */
  protected $query;

  /**
   * A mock Xero client.
   *
   * @var \Radcliffe\Xero\XeroClient|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $client;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'serialization',
    'user',
    'xero',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->client = $this->createMock('\Radcliffe\Xero\XeroClient');

    $this->query = new XeroQuery($this->client, \Drupal::service('serializer'), \Drupal::service('typed_data_manager'), \Drupal::service('logger.factory'), \Drupal::service('cache.xero_query'));

    // Throwing errors makes debugging easier.
    $this->query->throwErrors();
  }

  /**
   * Assert that getting a single item by id works.
   */
  public function testGetById() {
    $guid = "7cf47fe2-c3dd-4c6b-9895-7ba767ba529c";

    $this->query->setType('xero_user');
    $this->query->setId($guid);

    $options = $this->query->getOptions();
    $this->client->expects($this->once())
      ->method('request')
      ->with("get", "Users/$guid", $options)
      ->willReturn($this->getMockUserResponse());

    $list = $this->query->execute();
    $this->assertInstanceOf('\Drupal\xero\Plugin\DataType\XeroItemList', $list);
    $item = $list->get(0);
    $this->assertInstanceOf('\Drupal\xero\TypedData\XeroComplexItemInterface', $item);
  }

  /**
   * Assert that getting an item by condition works.
   */
  public function testGetByConditions() {
    $this->query->setType('xero_user');
    $this->query->addCondition('EmailAddress', "john.smith@mail.com");

    $options = $this->query->getOptions();
    $options['query']['where'] = 'EmailAddress=="john.smith@mail.com"';
    $this->client->expects($this->once())
      ->method('request')
      ->with("get", "Users", $options)
      ->willReturn($this->getMockUserResponse());

    $list = $this->query->execute();
    $this->assertInstanceOf('\Drupal\xero\Plugin\DataType\XeroItemList', $list);
    $item = $list->get(0);
    $this->assertInstanceOf('\Drupal\xero\TypedData\XeroComplexItemInterface', $item);
  }

  /**
   * Assert that getCache() method works.
   */
  public function testGetCache() {
    $this->query->setType('xero_user');
    $options = $this->query->getOptions();
    $this->client->expects($this->once())
      ->method('request')
      ->with("get", "Users", $options)
      ->willReturn($this->getMockUserResponse());

    $list = $this->query->getCache('xero_user');
    $this->assertInstanceOf('\Drupal\xero\Plugin\DataType\XeroItemList', $list);
    $item = $list->get(0);
    $this->assertInstanceOf('\Drupal\xero\TypedData\XeroComplexItemInterface', $item);

    // Second call to getCache() should get same result without invoking
    // client again.
    // @todo This is not currently testable as the cache is invalidated immediately
    // in XeroQuery::setCache().
    // $newList = $this->query->getCache('xero_user');
    // $this->assertSame($list, $newList);
  }

}
