<?php

namespace Drupal\Tests\xero\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\xero\XeroQuery;

/**
 * Tests the Xero query factory.
 *
 * @coversDefaultClass \Drupal\xero\XeroQueryFactory
 * @group xero
 */
class XeroQueryFactoryTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'serialization',
    'user',
    'xero',
  ];

  /**
   * Test the query factory works with a misconfigured client.
   */
  public function testClientWithoutConfiguration() {
    $queryFactory = \Drupal::service('xero.query.factory');
    $query = $queryFactory->get();
    $this->assertInstanceOf(XeroQuery::class, $query);
  }

}
