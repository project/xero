<?php

namespace Drupal\Tests\xero\Kernel\Normalizer;

use Drupal\Core\TypedData\TypedDataTrait;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the xero list normalizer with nested typed data.
 *
 * @group xero
 */
class XeroListNestedNormalizerTest extends KernelTestBase {

  use TypedDataTrait;

  /**
   * List items to normalize.
   *
   * @var \Drupal\xero\Plugin\DataType\XeroItemList
   */
  protected $items;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['user', 'serialization', 'xero'];

  /**
   * Asserts that typed data expands to the structured array that Xero expects.
   *
   * @param array $expected
   *   The expected output.
   * @param array $values
   *   The values to set on the typed data object.
   * @param string $format
   *   The serialization format string.
   * @param string $data_type
   *   The Typed Data data type plugin ID.
   *
   * @dataProvider normalizerDataProvider
   */
  public function testNormalize(array $expected, array $values, $format, $data_type) {
    $typedDataManager = $this->getTypedDataManager();
    $definition = $typedDataManager
      ->createListDataDefinition($data_type);
    $items = $typedDataManager->create($definition, $values);
    $data = \Drupal::service('serializer')->normalize($items, $format, ['plugin_id' => $data_type]);
    $this->assertEquals($expected, $data);
  }

  /**
   * Provides data for testNormalize().
   *
   * @return array
   *   An array of test parameters.
   */
  public static function normalizerDataProvider() {
    $valuesOne = [
      [
        'Name' => 'ABC Company',
        'FirstName' => 'Sam',
        'LastName' => 'Gonzaga',
        'Addresses' => [
          [
            'AddressType' => 'STREET',
            'AddressLine1' => 'Corso Vittorio Emanuele, 95',
            'City' => 'Quarrata',
            'Region' => 'PT',
            'PostalCode' => '51039',
            'Country' => 'Italy',
          ],
        ],
        'Phones' => [
          [
            'PhoneType' => 'DEFAULT',
            'PhoneNumber' => '7652874',
            'PhoneAreaCode' => '0359',
            'PhoneCountryCode' => '39',
          ],
          [
            'PhoneType' => 'MOBILE',
            'PhoneNumber' => '7492515',
            'PhoneAreaCode' => '0388',
            'PhoneCountryCode' => '39',
          ],
        ],
        'UpdatedDateUTC' => '2009-05-14T01:44:26.747',
      ],
    ];
    $valuesMultiple = [
          [
            'Name' => 'ABC Company',
            'FirstName' => 'Sam',
            'LastName' => 'Gonzaga',
          ],
          [
            'Name' => 'DEF Company',
            'FirstName' => 'Alex',
            'LastName' => 'Pirozzi',
          ],
    ];

    $jsonExpectOne = [
      'Name' => 'ABC Company',
      'FirstName' => 'Sam',
      'LastName' => 'Gonzaga',
      'Addresses' => [
        [
          'AddressType' => 'STREET',
          'AddressLine1' => 'Corso Vittorio Emanuele, 95',
          'City' => 'Quarrata',
          'Region' => 'PT',
          'PostalCode' => '51039',
          'Country' => 'Italy',
        ],
      ],
      'Phones' => [
        [
          'PhoneType' => 'DEFAULT',
          'PhoneNumber' => '7652874',
          'PhoneAreaCode' => '0359',
          'PhoneCountryCode' => '39',
        ],
        [
          'PhoneType' => 'MOBILE',
          'PhoneNumber' => '7492515',
          'PhoneAreaCode' => '0388',
          'PhoneCountryCode' => '39',
        ],
      ],
    ];

    $jsonExpectMultiple = [
      'Contacts' => [
        [
          'Name' => 'ABC Company',
          'FirstName' => 'Sam',
          'LastName' => 'Gonzaga',
        ],
        [
          'Name' => 'DEF Company',
          'FirstName' => 'Alex',
          'LastName' => 'Pirozzi',
        ],
      ],
    ];

    $lineItemExpectOne = [
      'Description' => 'A product',
      'Quantity' => 5.0,
      'UnitAmount' => 1.99,
      'ItemCode' => 'Whatever',
      'AccountCode' => '200',
      'Tracking' => [
        [
          'Name' => 'Region',
          'Option' => 'West Coast',
        ],
      ],
    ];
    $lineItemValuesOne = [
      [
        'Description' => 'A product',
        'Quantity' => 5.0,
        'UnitAmount' => 1.99,
        'ItemCode' => 'Whatever',
        'AccountCode' => '200',
        'Tracking' => [
          ['Name' => 'Region', 'Option' => 'West Coast'],
        ],
      ],
    ];

    return [
      [$jsonExpectOne, $valuesOne, 'json', 'xero_contact'],
      [
        $jsonExpectMultiple,
        $valuesMultiple,
        'json',
        'xero_contact',
      ],
      [$lineItemExpectOne, $lineItemValuesOne, 'json', 'xero_line_item'],
    ];
  }

  /**
   * Tests serialization of Xero contact data under different data scenarios.
   *
   * @param string $jsonExpected
   *   The expected serialized json.
   * @param array $data
   *   The data for the typed data object to be serialized.
   * @param string $message
   *   An optional message to display for the test.
   *
   * @dataProvider serializationProvider
   */
  public function testSerialization($jsonExpected, array $data, $message) {
    $typedDataManager = $this->getTypedDataManager();
    $definition = $typedDataManager
      ->createListDataDefinition('xero_contact');
    $items = $typedDataManager->create($definition, $data);

    $serializer = \Drupal::service('serializer');
    $context = ['plugin_id' => 'xero_contact'];
    $json = $serializer->serialize($items, 'json', $context);
    $this->assertEquals($jsonExpected, $json, $message . " (json)");
  }

  /**
   * Provides test arguments for testSerialization.
   *
   * @return array
   *   An indexed array of tests to run with test arguments.
   */
  public static function serializationProvider() {
    return [
      [
        '{"Name":"ABC Limited","FirstName":"","LastName":null,"Phones":[]}',
        [
          [
            'Name' => 'ABC Limited',
            'FirstName' => '',
            'LastName' => NULL,
            'Phones' => [],
          ],
        ],
        'serializer uses normalizer and handles empty values',
      ],
    ];
  }

}
