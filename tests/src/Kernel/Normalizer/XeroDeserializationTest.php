<?php

namespace Drupal\Tests\xero\Kernel\Normalizer;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\xero\Traits\XeroGuidTrait;
use Drupal\Tests\xero\Traits\XeroResponseTrait;
use GuzzleHttp\Psr7\Response;

/**
 * Tests deserialization of Xero responses into typed data.
 *
 * @group xero
 */
class XeroDeserializationTest extends KernelTestBase {

  use XeroGuidTrait;
  use XeroResponseTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['user', 'serialization', 'xero'];

  /**
   * Test deserialization for contacts.
   */
  public function testDeserializeContact() {
    $item = $this->deserializeResponseItem('xero_contact', $this->getMockContactResponse());
    $values = $item->getValue();
    $this->assertNotEmpty($values, "The mock user response should have been deserialized into an array.");
    $this->assertArrayHasValue('7cf47fe2-c3dd-4c6b-9895-7ba767ba529c', 'ContactID', $values);
    $this->assertArrayHasValue('John Smith', 'Name', $values);
    $this->assertArrayHasValue('John', 'FirstName', $values);
    $this->assertArrayHasValue('Smith', 'LastName', $values);
    $this->assertArrayHasValue('john.smith@mail.com', 'EmailAddress', $values);
    $this->assertArrayHasValue(TRUE, 'IsCustomer', $values);
    $this->assertArrayHasValue(FALSE, 'IsSupplier', $values);
  }

  /**
   * Test deserialization for invoices, especially dates.
   */
  public function testDeserializeInvoice() {
    $item = $this->deserializeResponseItem('xero_invoice', $this->getMockInvoiceResponse());
    $values = $item->getValue();
    $this->assertNotEmpty($values, "The mock invoice response should have been deserialized into an array.");

    $this->assertArrayHasValue('ACCREC', 'Type', $values);
    $this->assertArrayHasValue('2025.00', 'Total', $values);
    $this->assertArrayHasValue('2009-05-27T00:00:00+00:00', 'Date', $values);
    $this->assertArrayHasValue('2009-06-06T00:00:00+00:00', 'DueDate', $values);
    $this->assertArrayHasValue('2009-06-05T12:00:00+00:00', 'ExpectedPaymentDate', $values);

    $this->assertArrayHasKey('LineItems', $values);
    $this->assertCount(1, $values['LineItems']);
    $this->assertArrayHasValue('12', 'ItemCode', $values['LineItems'][0]);

    $this->assertArrayHasKey('Contact', $values);
    $this->assertArrayHasValue('025867f1-d741-4d6b-b1af-9ac774b59ba7', 'ContactID', $values['Contact']);
    $this->assertArrayHasValue('City Agency', 'Name', $values['Contact']);

    // @todo assert once https://www.drupal.org/project/xero/issues/3242097 is fixed.
    /*
     * Drupal.Files.LineLength.TooLong should not require someone to do this for
     * commented out code. DrupalWTF.
     * $this->assertArrayHasValue(
     *   '2009-04-13T14:02:12+00:00',
     *   'UpdatedDateUTC',
     *   $values
     * );
     * $this->assertArrayHasValue(
     *   '2009-04-24T07:17:34+00:00',
     *   'UpdatedDateUTC',
     *   $values['Contact']
     * );
     */
  }

  /**
   * Extract a single Xero item from a Xero response.
   *
   * @param string $type
   *   The type of Xero item.
   * @param \GuzzleHttp\Psr7\Response $response
   *   The mock Xero response.
   * @param int $index
   *   (optional) The index of the Xero item to extract.
   *
   * @return \Drupal\xero\TypedData\XeroComplexItemInterface
   *   A Xero item.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function deserializeResponseItem($type, Response $response, $index = 0) {
    $list = $this->deserializeResponseItems($type, $response);
    $item = $list->get($index);
    $this->assertInstanceOf('\Drupal\xero\TypedData\XeroComplexItemInterface', $item);
    return $item;
  }

  /**
   * Deserialize a Xero response into a Xero item list.
   *
   * @param string $type
   *   The type of Xero item.
   * @param \GuzzleHttp\Psr7\Response $response
   *   The mock Xero response.
   *
   * @return \Drupal\xero\Plugin\DataType\XeroItemList
   *   A list of Xero items.
   */
  protected function deserializeResponseItems($type, Response $response) {
    $list = \Drupal::service('serializer')->deserialize(
      $response->getBody()->getContents(),
      \Drupal::service('typed_data_manager')->getDefinition($type)['class'],
      'json',
      ['plugin_id' => $type]
    );
    $this->assertInstanceOf('\Drupal\xero\Plugin\DataType\XeroItemList', $list);
    return $list;
  }

  /**
   * Assert an array has a key with a value.
   *
   * @param mixed $expected
   *   The expected value.
   * @param mixed $key
   *   The expected key.
   * @param array $array
   *   The actual array.
   */
  protected function assertArrayHasValue($expected, $key, array $array) {
    $this->assertArrayHasKey($key, $array);
    $this->assertSame($expected, $array[$key]);
  }

}
