<?php

namespace Drupal\Tests\xero\Kernel\Normalizer;

use Drupal\Core\TypedData\TypedDataTrait;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests denormalizer with services enabled.
 *
 * @group xero
 */
class XeroItemTest extends KernelTestBase {

  use TypedDataTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['user', 'serialization', 'xero'];

  /**
   * Tests the normalization of a Xero type under different data scenarios.
   */
  public function testGetSpecifiedProperties() {
    $definition = $this->getTypedDataManager()
      ->createDataDefinition('xero_contact');

    // Test properties set at item creation are specified.
    $data = [
      'EmailAddress' => 'John@example.com',
      'Name' => 'John Doe',
      'Addresses' => [
        [
          'AddressLine1' => '500 Main Street',
          'City' => 'Wellington',
          'PostalCode' => '6011',
        ],
      ],
    ];
    /** @var \Drupal\xero\TypedData\XeroItemInterface $item */
    $item = $this->getTypedDataManager()->create($definition, $data, 'Contact');
    $set = array_keys($item->getSpecifiedProperties());
    sort($set);
    $this->assertEquals(['Addresses', 'EmailAddress', 'Name'], $set);

    // Test properties explicitly set subsequently are specified.
    $item->get('ContactNumber')->setValue('123');
    $set = array_keys($item->getSpecifiedProperties());
    sort($set);
    $this->assertEquals(['Addresses', 'ContactNumber', 'EmailAddress', 'Name'], $set);

    // Test applying default values has no effect.
    $item->applyDefaultValue();
    $set = array_keys($item->getSpecifiedProperties());
    sort($set);
    $this->assertEquals(['Addresses', 'ContactNumber', 'EmailAddress', 'Name'], $set);

    // Test validation has no effect.
    $item->validate();
    $set = array_keys($item->getSpecifiedProperties());
    sort($set);
    $this->assertSame(['Addresses', 'ContactNumber', 'EmailAddress', 'Name'], $set);

    // Test the specified properties remain correct when retrieved from list.
    $listDefinition = $this->getTypedDataManager()
      ->createListDataDefinition('xero_contact');
    $list = $this->getTypedDataManager()->create($listDefinition);
    $list->offsetSet(0, $item);
    $item = $list->get(0);
    $set = array_keys($item->getSpecifiedProperties());
    sort($set);
    $this->assertSame(['Addresses', 'ContactNumber', 'EmailAddress', 'Name'], $set);

    // Test setting to pristine resets the specified properties to only required
    // properties.
    $item->markAsPristine();
    $set = array_keys($item->getSpecifiedProperties());
    $this->assertEquals(['Name'], $set);

    // Test setting a property has only the changed property.
    $item->get('ContactNumber')->setValue('456');
    $set = array_keys($item->getSpecifiedProperties());
    $this->assertEquals(['ContactNumber', 'Name'], $set);

    // Test setting a property of a child property.
    $item
      ->get('Addresses')
      ->set(0, ['AddressLine' => '10 High Street']);
    $set = array_keys($item->getSpecifiedProperties());
    sort($set);
    $this->assertEquals(['Addresses', 'ContactNumber', 'Name'], $set);
  }

}
