<?php

namespace Drupal\Tests\xero\Kernel\Plugin\DataType;

use Drupal\Core\TypedData\TypedDataTrait;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\xero\Traits\XeroGuidTrait;
use Drupal\xero\Plugin\DataType\Account;
use Drupal\xero\Plugin\DataType\Address;
use Drupal\xero\Plugin\DataType\BankTransaction;
use Drupal\xero\Plugin\DataType\BankTransfer;
use Drupal\xero\Plugin\DataType\BrandingTheme;
use Drupal\xero\Plugin\DataType\Contact;
use Drupal\xero\Plugin\DataType\ContactGroup;
use Drupal\xero\Plugin\DataType\CreditNote;
use Drupal\xero\Plugin\DataType\Currency;
use Drupal\xero\Plugin\DataType\Detail;
use Drupal\xero\Plugin\DataType\Employee;
use Drupal\xero\Plugin\DataType\Expense;
use Drupal\xero\Plugin\DataType\Invoice;
use Drupal\xero\Plugin\DataType\InvoiceReminder;
use Drupal\xero\Plugin\DataType\Item;
use Drupal\xero\Plugin\DataType\Journal;
use Drupal\xero\Plugin\DataType\JournalLine;
use Drupal\xero\Plugin\DataType\LineItem;
use Drupal\xero\Plugin\DataType\Link;
use Drupal\xero\Plugin\DataType\LinkedTransaction;
use Drupal\xero\Plugin\DataType\Organisation;
use Drupal\xero\Plugin\DataType\Payment;
use Drupal\xero\Plugin\DataType\Phone;
use Drupal\xero\Plugin\DataType\Receipt;
use Drupal\xero\Plugin\DataType\RepeatingInvoice;
use Drupal\xero\Plugin\DataType\Schedule;
use Drupal\xero\Plugin\DataType\TaxComponent;
use Drupal\xero\Plugin\DataType\TaxRate;
use Drupal\xero\Plugin\DataType\TrackingCategory;
use Drupal\xero\Plugin\DataType\TrackingOption;
use Drupal\xero\Plugin\DataType\User;
use Drupal\xero\TypedData\Definition\AccountDefinition;
use Drupal\xero\TypedData\Definition\AddressDefinition;
use Drupal\xero\TypedData\Definition\BankTransactionDefinition;
use Drupal\xero\TypedData\Definition\BankTransferDefinition;
use Drupal\xero\TypedData\Definition\BrandingThemeDefinition;
use Drupal\xero\TypedData\Definition\ContactDefinition;
use Drupal\xero\TypedData\Definition\ContactGroupDefinition;
use Drupal\xero\TypedData\Definition\CreditDefinition;
use Drupal\xero\TypedData\Definition\CurrencyDefinition;
use Drupal\xero\TypedData\Definition\DetailDefinition;
use Drupal\xero\TypedData\Definition\EmployeeDefinition;
use Drupal\xero\TypedData\Definition\ExpenseDefinition;
use Drupal\xero\TypedData\Definition\InvoiceDefinition;
use Drupal\xero\TypedData\Definition\InvoiceReminderDefinition;
use Drupal\xero\TypedData\Definition\ItemDefinition;
use Drupal\xero\TypedData\Definition\JournalDefinition;
use Drupal\xero\TypedData\Definition\JournalLineDefinition;
use Drupal\xero\TypedData\Definition\LineItemDefinition;
use Drupal\xero\TypedData\Definition\LinkDefinition;
use Drupal\xero\TypedData\Definition\LinkedTransactionDefinition;
use Drupal\xero\TypedData\Definition\OrganisationDefinition;
use Drupal\xero\TypedData\Definition\PaymentDefinition;
use Drupal\xero\TypedData\Definition\PhoneDefinition;
use Drupal\xero\TypedData\Definition\ReceiptDefinition;
use Drupal\xero\TypedData\Definition\RepeatingInvoiceDefinition;
use Drupal\xero\TypedData\Definition\ScheduleDefinition;
use Drupal\xero\TypedData\Definition\TaxComponentDefinition;
use Drupal\xero\TypedData\Definition\TaxRateDefinition;
use Drupal\xero\TypedData\Definition\TrackingCategoryDefinition;
use Drupal\xero\TypedData\Definition\TrackingOptionDefinition;
use Drupal\xero\TypedData\Definition\UserDefinition;

/**
 * Tests typed data for xero data types.
 *
 * @group xero
 */
class XeroDataTypeTest extends KernelTestBase {

  use TypedDataTrait;
  use XeroGuidTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['user', 'serialization', 'xero'];

  /**
   * Tests each data type has correct data definition class.
   *
   * @param string $plugin_id
   *   The data type plugin id.
   * @param string $expectedClass
   *   The expected class of the data type plugin.
   * @param string $expectedDefinition
   *   The expected class of the data definition.
   *
   * @dataProvider dataDefinitionTestProvider
   */
  public function testDataDefinition(string $plugin_id, string $expectedClass, string $expectedDefinition) {
    $typedDataManager = $this->getTypedDataManager();
    $configuration = [
      'name' => NULL,
      'parent' => NULL,
      'data_definition' => $typedDataManager->createDataDefinition($plugin_id),
    ];
    $item = $typedDataManager->createInstance($plugin_id, $configuration);

    $this->assertInstanceOf($expectedClass, $item);
    $this->assertInstanceOf($expectedDefinition, $item->getDataDefinition());
  }

  /**
   * Tests instantiating a xero data type.
   *
   * @param string $plugin_id
   *   The data type plugin id.
   * @param array<string,mixed> $values
   *   The data to set on the data type.
   *
   * @dataProvider dataTypeTestProvider
   */
  public function testDataType(string $plugin_id, array $values) {
    $typedDataManager = $this->getTypedDataManager();
    $definition = $typedDataManager->createDataDefinition($plugin_id);
    $item = $typedDataManager->create($definition, $values);

    $this->assertEquals($values, $item->getValue());
  }

  /**
   * Provides test cases for data definition test.
   *
   * @return array<int,mixed>
   *   An array of test case arguments.
   */
  public static function dataDefinitionTestProvider() {
    return [
      ['xero_account', Account::class, AccountDefinition::class],
      ['xero_address', Address::class, AddressDefinition::class],
      [
        'xero_bank_transaction',
        BankTransaction::class,
        BankTransactionDefinition::class,
      ],
      [
        'xero_bank_transfer',
        BankTransfer::class,
        BankTransferDefinition::class,
      ],
      [
        'xero_branding_theme',
        BrandingTheme::class,
        BrandingThemeDefinition::class,
      ],
      ['xero_contact', Contact::class, ContactDefinition::class],
      [
        'xero_contact_group',
        ContactGroup::class,
        ContactGroupDefinition::class,
      ],
      ['xero_credit_note', CreditNote::class, CreditDefinition::class],
      ['xero_currency', Currency::class, CurrencyDefinition::class],
      ['xero_detail', Detail::class, DetailDefinition::class],
      ['xero_employee', Employee::class, EmployeeDefinition::class],
      ['xero_expense', Expense::class, ExpenseDefinition::class],
      ['xero_invoice', Invoice::class, InvoiceDefinition::class],
      [
        'xero_invoice_reminder',
        InvoiceReminder::class,
        InvoiceReminderDefinition::class,
      ],
      ['xero_item', Item::class, ItemDefinition::class],
      ['xero_journal', Journal::class, JournalDefinition::class],
      ['xero_journal_line', JournalLine::class, JournalLineDefinition::class],
      ['xero_line_item', LineItem::class, LineItemDefinition::class],
      ['xero_link', Link::class, LinkDefinition::class],
      [
        'xero_linked_transaction',
        LinkedTransaction::class,
        LinkedTransactionDefinition::class,
      ],
      [
        'xero_organisation',
        Organisation::class,
        OrganisationDefinition::class,
      ],
      ['xero_payment', Payment::class, PaymentDefinition::class],
      ['xero_phone', Phone::class, PhoneDefinition::class],
      ['xero_receipt', Receipt::class, ReceiptDefinition::class],
      [
        'xero_repeating_invoice',
        RepeatingInvoice::class,
        RepeatingInvoiceDefinition::class,
      ],
      ['xero_schedule', Schedule::class, ScheduleDefinition::class],
      [
        'xero_tax_component',
        TaxComponent::class,
        TaxComponentDefinition::class,
      ],
      ['xero_tax_rate', TaxRate::class, TaxRateDefinition::class],
      [
        'xero_tracking',
        TrackingCategory::class,
        TrackingCategoryDefinition::class,
      ],
      [
        'xero_tracking_option',
        TrackingOption::class,
        TrackingOptionDefinition::class,
      ],
      ['xero_user', User::class, UserDefinition::class],
    ];
  }

  /**
   * Provides test cases for data type test.
   *
   * @return array<string,mixed>
   *   An array of test case arguments.
   */
  public static function dataTypeTestProvider(): array {
    return [
      'Contact (basic)' => [
        'xero_contact',
        [
          'ContactID' => static::createGuid(),
          'ContactNumber' => '00005',
          'Name' => 'Pat',
          'FirstName' => 'Pat',
          'LastName' => 'Example',
        ],
      ],
      'Contact (branded)' => [
        'xero_contact',
        [
          'ContactID' => static::createGuid(),
          'ContactNumber' => '00004',
          'Name' => 'Sam',
          'FirstName' => 'Sam',
          'LastName' => 'Example',
          'BrandingTheme' => [
            'Name' => 'Very orange invoice!',
          ],
        ],
      ],
      'Contact (all)' => [
        'xero_contact',
        [
          'ContactID' => static::createGuid(),
          'ContactNumber' => '00005',
          'AccountNumber' => '200',
          'Name' => 'Pat',
          'FirstName' => 'Pat',
          'LastName' => 'Example',
          'EmailAddress' => 'pat@example.com',
          'SkypeUserName' => 'pat@example.com',
          'BankAccountDetails' => '41565456454',
          'TaxNumber' => '415465456454',
          'AccountsReceivableTaxType' => 'INPUT2',
          'AccountsPayableTaxType' => 'OUTPUT2',
          'Addresses' => [
            [
              'AddressType' => 'POBOX',
              'AddressLine1' => 'P O Box 123',
              'City' => 'Welington',
              'PostalCode' => '6011',
              'AttentionTo' => 'Pat',
            ],
          ],
          'Phones' => [
            [
              'PhoneType' => 'DEFAULT',
              'PhoneNumber' => '1111111',
              'PhoneAreaCode' => '04',
              'PhoneCountryCode' => '64',
            ],
          ],
          'IsSupplier' => FALSE,
          'IsCustomer' => TRUE,
          'DefaultCurrency' => 'NZD',
        ],
      ],
    ];
  }

}
