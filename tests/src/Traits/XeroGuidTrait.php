<?php

namespace Drupal\Tests\xero\Traits;

use Drupal\Component\Utility\Random;

/**
 * A trait for woking with Xero Guids in tests.
 */
trait XeroGuidTrait {

  /**
   * Create a Guid.
   *
   * @return string
   *   A valid globally-unique identifier.
   */
  public static function createGuid() {
    $randomGenerator = new Random();
    $hash = strtoupper(hash('ripemd128', md5($randomGenerator->string(100))));
    $guid = substr($hash, 0, 8) . '-' . substr($hash, 8, 4) . '-' . substr($hash, 12, 4);
    $guid .= '-' . substr($hash, 16, 4) . '-' . substr($hash, 20, 12);

    // A Guid string representation should be output as lower case per UUIDs
    // and GUIDs Network Working Group INTERNET-DRAFT 3.3.
    return strtolower($guid);
  }

  /**
   * Create a Guid wrapped in braces.
   *
   * @return string
   *   A valid globally-unique identifier, wrapped in braces.
   */
  public static function createGuidWithBraces() {
    return '{' . static::createGuid() . '}';
  }

}
