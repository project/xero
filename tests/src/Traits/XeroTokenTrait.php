<?php

namespace Drupal\Tests\xero\Traits;

use Drupal\Component\Utility\Random;

/**
 * A trait for working with tokens in Xero tests.
 */
trait XeroTokenTrait {

  /**
   * Create a mock consumer key or secret.
   *
   * @return string
   *   A 30 character string that can be used as a consumer key or secret.
   */
  public static function createToken() {
    $random = new Random();
    return strtoupper(hash('ripemd128', md5($random->string(30))));
  }

}
