<?php

namespace Drupal\Tests\xero\Traits;

use GuzzleHttp\Psr7\Response;

/**
 * A trait that provides mock responses for Xero tests.
 */
trait XeroResponseTrait {

  /**
   * A mock response to the users endpoint with a single user.
   *
   * @return \GuzzleHttp\Psr7\Response
   *   A mock response object with json content.
   */
  protected function getMockUserResponse() {
    $json = '{
    "Id": "' . self::createGuid() . '",
    "Status": "OK",
    "ProviderName": "Drupal Xero",
    "DateTimeUTC": "\/Date(1632057190856)\/",
    "Users": [
      {
        "UserID": "7cf47fe2-c3dd-4c6b-9895-7ba767ba529c",
        "Name": "John Smith",
        "FirstName": "John",
        "LastName": "Smith",
        "EmailAddress": "john.smith@mail.com",
        "UpdatedDateUTC": "\/Date(1615463133377+0000)\/",
        "IsSubscriber": true,
        "OrganisationRole": "ADMIN"
      }
    ]
  }';

    return new Response(
      200,
      [
        'Content-Type' => 'text/json',
      ],
      $json
    );
  }

  /**
   * A mock response to the users endpoint with a single user.
   *
   * @return \GuzzleHttp\Psr7\Response
   *   A mock response object with json content.
   */
  protected function getMockContactResponse() {
    $json = '{
    "Id": "' . $this->createGuid(FALSE) . '",
    "Status": "OK",
    "ProviderName": "Drupal Xero",
    "DateTimeUTC": "\/Date(1632057190856)\/",
    "Contacts": [
      {
        "ContactID": "7cf47fe2-c3dd-4c6b-9895-7ba767ba529c",
        "Name": "John Smith",
        "FirstName": "John",
        "LastName": "Smith",
        "EmailAddress": "john.smith@mail.com",
        "UpdatedDateUTC": "\/Date(1615463133377+0000)\/",
        "IsCustomer": true,
        "IsSupplier": false
      }
    ]
  }';

    return new Response(
      200,
      [
        'Content-Type' => 'text/json',
      ],
      $json
    );
  }

  /**
   * A mock response to the invoices endpoint with a single invoice.
   *
   * @return \GuzzleHttp\Psr7\Response
   *   A mock response object with json content.
   */
  protected function getMockInvoiceResponse() {
    $json = '{
      "Id": "' . self::createGuid() . '",
      "Status": "OK",
      "ProviderName": "Drupal Xero",
      "DateTimeUTC": "\/Date(1632057190856)\/",
      "Invoices": [
        {
          "Type": "ACCREC",
          "Contact": {
            "ContactID": "025867f1-d741-4d6b-b1af-9ac774b59ba7",
            "Name": "City Agency",
            "UpdatedDateUTC": "\/Date(1240557454000+0000)\/"
          },
          "Date": "\/Date(1243382400000+0000)\/",
          "DueDate": "\/Date(1244246400000+0000)\/",
          "ExpectedPaymentDate": "\/Date(1244203200000+0000)\/",
          "LineItems": [
            {
              "ItemCode": "12",
              "Description": "Onsite project management ",
              "Quantity": "1.0000",
              "UnitAmount": "1800.00",
              "TaxType": "OUTPUT",
              "TaxAmount": "225.00",
              "LineAmount": "1800.00",
              "AccountCode": "200",
              "LineItemID": "52208ff9-528a-4985-a9ad-b2b1d4210e38"
            }
          ],
          "Total": "2025.00",
          "UpdatedDateUTC": "\/Date(1239631332000+0000)\/"
          }
        ]
    }';

    return new Response(
      200,
      [
        'Content-Type' => 'text/json',
      ],
      $json
    );
  }

}
