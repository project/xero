<?php

namespace Drupal\xero\TypedData;

use Drupal\Core\TypedData\ComplexDataInterface;

/**
 * Represents a basic Xero data type.
 *
 * @see \Drupal\xero\Plugin\DataType\XeroItemBase
 */
interface XeroItemInterface extends ComplexDataInterface {

  /**
   * Gets one of the xero static properties by name.
   *
   * @param string $name
   *   The name of a static property on the class. This should be one of:
   *     - guid_name
   *     - xero_name
   *     - plural_name
   *     - label_name
   *   Not all data types use all properties.
   *
   * @return string|null
   *   The value of the xero property.
   *
   * @throws \InvalidArgumentException
   */
  public static function getXeroProperty($name);

  /**
   * Render the typed data into a render element.
   *
   * @return array
   *   A render array.
   */
  public function view(): array;

  /**
   * Gets an array of the property objects that have been explicitly specified.
   *
   * The properties defined for this typed data object are filtered to include
   * only those properties that have so far been explicitly specified. By
   * definition, this does not include computed properties.
   *
   * @return array|\Drupal\Core\TypedData\TypedDataInterface[]
   *   An array of property objects implementing TypedDataInterface, keyed by
   *   property name.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   *   If the complex data structure is unset and no property can be created.
   */
  public function getSpecifiedProperties();

  /**
   * Checks if the values have been modified.
   *
   * @return bool
   *   TRUE if any property has been modified.
   */
  public function isPristine();

  /**
   * Resets the data type to be pristine for all properties.
   *
   * @param bool $notify
   *   Notifies the parent to reset this property when the parent implements
   *   this interface or the item list's parent implements this interface.
   */
  public function markAsPristine($notify = TRUE);

}
