<?php

namespace Drupal\xero\TypedData\Definition;

use Drupal\Core\TypedData\ComplexDataDefinitionBase;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\xero\TaxTypeTrait;

/**
 * Xero Item data definition.
 */
class ItemDefinition extends ComplexDataDefinitionBase implements XeroDefinitionInterface {

  use TaxTypeTrait;

  /**
   * {@inheritdoc}
   *
   * @todo additional properties for items - http://developer.xero.com/documentation/api/items/
   */
  public function getPropertyDefinitions() {
    if (!isset($this->propertyDefinitions)) {
      $info = &$this->propertyDefinitions;

      $info['ItemID'] = DataDefinition::create('string')
        ->setLabel('Item ID')
        ->setReadOnly(TRUE)
        ->addConstraint('XeroGuidConstraint');

      // Writeable.
      $info['Code'] = DataDefinition::create('string')
        ->setRequired(TRUE)
        ->setLabel('Code')
        ->addConstraint('Length', ['max' => 30]);
      $info['Name'] = DataDefinition::create('string')->setLabel('Name');

      // Recommended.
      $info['Description'] = DataDefinition::create('string')->setLabel('Description');
      $info['PurchaseDescription'] = DataDefinition::create('string')->setLabel('Purchase Description');
      $info['PurchaseDetails'] = DataDefinition::create('xero_detail')->setLabel('Purchase Details');
      $info['SalesDetails'] = DataDefinition::create('xero_detail')->setLabel('Sales Details');

      $info['IsSold'] = DataDefinition::create('boolean')->setLabel('Is Sold?');
      $info['IsPurchased'] = DataDefinition::create('boolean')->setLabel('Is Purchased?');
      $info['IsTrackedAsInventory'] = DataDefinition::create('boolean')->setLabel('Is Tracked As Inventory?');
      $info['InventoryAssetAccountCode'] = DataDefinition::create('string')->setLabel('Inventory Asset Account Code');
      $info['TotalCostPool'] = DataDefinition::create('float')->setLabel('Total Cost Pool');
      $info['QuantityOnHand'] = DataDefinition::create('float')->setLabel('Quantity On Hand');
      $info['UpdatedDateUTC'] = DataDefinition::create('datetime_iso8601')
        ->setLabel('Updated Date')
        ->setReadOnly(TRUE);

      $info['UnitPrice'] = DataDefinition::create('float')->setLabel('Unit Price');
      $info['AccountCode'] = DataDefinition::create('string')->setLabel('Account Code');
      $info['COGSAccountCode'] = DataDefinition::create('string')->setLabel('COGS Account Code');
      $info['TaxType'] = DataDefinition::create('string')
        ->setLabel('Tax Type')
        ->addConstraint('XeroChoiceConstraint', ['choices' => self::getTaxTypes()]);

    }
    return $this->propertyDefinitions;
  }

}
