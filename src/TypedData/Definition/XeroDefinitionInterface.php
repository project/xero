<?php

namespace Drupal\xero\TypedData\Definition;

use Drupal\Core\TypedData\ComplexDataDefinitionInterface;

/**
 * Describes the property definitions for a Xero data type.
 *
 * This is an empty interface to isolate Xero data types from Drupal data types.
 */
interface XeroDefinitionInterface extends ComplexDataDefinitionInterface {}
