<?php

namespace Drupal\xero;

use Drupal\xero\TypedData\XeroComplexItemInterface;

/**
 * An interface for a service for managing Xero items.
 */
interface XeroItemManagerInterface {

  /**
   * Creates a single item on Xero.
   *
   * @param \Drupal\xero\TypedData\XeroComplexItemInterface $item
   *   The Xero item to create.
   *
   * @return \Drupal\xero\TypedData\XeroComplexItemInterface|bool
   *   A typed data representation of the Xero item, or FALSE if unsuccessful.
   */
  public function createItem(XeroComplexItemInterface $item): XeroComplexItemInterface|bool;

  /**
   * Finds a single Xero item by specified criteria.
   *
   * @param string $type
   *   The Xero data type.
   * @param array $conditions
   *   An array of criteria. Each array value must be a subarray that is a
   *   single criterion. See /Drupal/xero/src/XeroQuery::addCondition for the
   *   required subarray structure and available options.
   * @param bool $unique
   *   (optional) Whether the item returned needs to be the only match to the
   *   criteria. Defaults to FALSE, in which case the first item is returned if
   *   there are multiple. If TRUE then returns NULL if multiple items match
   *   the criteria.
   *
   * @return \Drupal\xero\TypedData\XeroComplexItemInterface|bool|null
   *   A typed data representation of a Xero item, or NULL if a match could not
   *   be identified, or FALSE if the query was unsuccessful.
   */
  public function findItem(string $type, array $conditions, bool $unique = FALSE): XeroComplexItemInterface|bool|null;

  /**
   * Loads a Xero item by type and guid.
   *
   * @param string $type
   *   The type of the Xero item.
   * @param string $guid
   *   The guid of the Xero item.
   *
   * @return \Drupal\xero\TypedData\XeroComplexItemInterface|bool
   *   A typed data representation of the Xero item, or FALSE if unsuccessful.
   */
  public function loadItem(string $type, string $guid): XeroComplexItemInterface|bool;

  /**
   * Gets fresh data on a Xero item.
   *
   * @param \Drupal\xero\TypedData\XeroComplexItemInterface $item
   *   The Xero item to refresh.
   *
   * @return \Drupal\xero\TypedData\XeroComplexItemInterface|bool
   *   A typed data representation of the Xero item, or FALSE if unsuccessful.
   */
  public function reloadItem(XeroComplexItemInterface $item): XeroComplexItemInterface|bool;

  /**
   * Updates a single item on Xero.
   *
   * @param \Drupal\xero\TypedData\XeroComplexItemInterface $item
   *   The Xero item to update.
   *
   * @return \Drupal\xero\TypedData\XeroComplexItemInterface|bool
   *   A typed data representation of the Xero item, or FALSE if unsuccessful
   */
  public function updateItem(XeroComplexItemInterface $item): XeroComplexItemInterface|bool;

  /**
   * Whether or not to throw errors generated when the query is executed.
   *
   * @param bool $shouldThrow
   *   Whether errors should be thrown.
   *
   * @return $this
   *   The query object for chaining.
   */
  public function throwErrors(bool $shouldThrow = TRUE): self;

}
