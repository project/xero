<?php

namespace Drupal\xero;

use Drupal\Core\Session\AccountProxyInterface;
use League\OAuth2\Client\Token\AccessTokenInterface;

/**
 * Describes the Xero token manager.
 *
 * This is used to set and get global and user-specified tokens.
 */
interface XeroTokenManagerInterface {

  /**
   * Get the available access tokens for the user account.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The user account to use when getting a user-specified access token (when
   *   the $global parameter is FALSE).
   * @param bool $global
   *   Gets the global access token if the provided user account has access to
   *   it.
   *
   * @return \League\OAuth2\Client\Token\AccessTokenInterface|bool
   *   An instantiated OAuth2 access token or FALSE if no access token is found
   *   associated with the current user.
   *
   * @throws \InvalidArgumentException
   */
  public function getToken(AccountProxyInterface $account, bool $global = TRUE): AccessTokenInterface|bool;

  /**
   * Gets the stored organizations the user has access to.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The user account to use when getting a user-specified access token (when
   *   the $global parameter is FALSE).
   *
   * @return array<string, mixed>
   *   An indexed array of organizations.
   */
  public function getUserOrganizations(AccountProxyInterface $account): array;

  /**
   * Sets a global or user-specified access token.
   *
   * This will also save organizations that the user has access to.
   *
   * @param \League\OAuth2\Client\Token\AccessTokenInterface $accessToken
   *   The OAuth2 access token.
   * @param \Drupal\Core\Session\AccountProxyInterface|null $account
   *   (optional) The user account to set the token for. This will set the
   *   global token when NULL or anonymous.
   * @param array<string, mixed> $organizations
   *   (optional) A list of organizations available to this user.
   */
  public function setToken(AccessTokenInterface $accessToken, ?AccountProxyInterface $account = NULL, array $organizations = []): void;

  /**
   * Determines if the user has a token stored.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The user account to check.
   *
   * @return bool
   *   Returns TRUE if the user has a per-user token stored.
   */
  public function hasUserToken(AccountProxyInterface $account): bool;

}
