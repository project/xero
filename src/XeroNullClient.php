<?php

namespace Drupal\xero;

use Drupal\xero\Exception\XeroInvalidConfigurationException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Promise\RejectedPromise;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;
use Radcliffe\Xero\XeroClientInterface;

/**
 * Provides a client that throws exceptions.
 *
 * @deprecated in xero:3.1.0-alpha1 and is removed from xero:4.0.0. Use
 *             XeroClient, which handles configuration errors as Guzzle
 *             middleware.
 * @see https://www.drupal.org/project/xero/issues/3454689
 */
class XeroNullClient implements XeroClientInterface {

  /**
   * The configuration used.
   *
   * @var array
   */
  protected $xeroConfiguration;

  /**
   * A list of valid tenant guids.
   *
   * @var string[]
   */
  protected $tenantIds = [];

  /**
   * The refresh token.
   *
   * @var string
   */
  protected $refreshedToken = NULL;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $config) {
    $this->xeroConfiguration = $config;
  }

  /**
   * Exception message used by all methods.
   *
   * @var string
   */
  public static $exceptionMessage = 'The Xero client is not configured correctly.';

  /**
   * {@inheritdoc}
   */
  public function send(RequestInterface $request, array $options = []): ResponseInterface {
    throw new RequestException(
      'Request could not be made',
      $request,
      NULL,
      new XeroInvalidConfigurationException(self::$exceptionMessage));
  }

  /**
   * {@inheritdoc}
   */
  public function sendAsync(RequestInterface $request, array $options = []): PromiseInterface {
    return new RejectedPromise(new XeroInvalidConfigurationException(self::$exceptionMessage));
  }

  /**
   * {@inheritdoc}
   */
  public function requestAsync($method, $uri = '', array $options = []): PromiseInterface {
    return new RejectedPromise(new XeroInvalidConfigurationException(self::$exceptionMessage));
  }

  /**
   * {@inheritdoc}
   */
  public function request($method, $uri = '', array $options = []): ResponseInterface {
    $headers = $options['headers'] ?? [];
    $body = $options['body'] ?? NULL;
    $request = new Request($method, $uri, $headers, $body);
    return $this->send($request, $options);
  }

  /**
   * Dummy magic method.
   *
   * @param string $method
   *   The method to call.
   * @param array $args
   *   The method arguments.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function __call($method, array $args) {
    if (count($args) < 1) {
      throw new \InvalidArgumentException('Magic request methods require a URI and optional options array');
    }

    $uri = $args[0];
    $opts = $args[1] ?? [];

    return str_ends_with($method, 'Async')
      ? $this->requestAsync(substr($method, 0, -5), $uri, $opts)
      : $this->request($method, $uri, $opts);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig($option = NULL) {
    throw new XeroInvalidConfigurationException(self::$exceptionMessage);
  }

  /**
   * {@inheritdoc}
   */
  public static function getValidUrls(): array {
    throw new XeroInvalidConfigurationException(self::$exceptionMessage);
  }

  /**
   * {@inheritdoc}
   */
  public function isValidUrl($base_uri): bool {
    throw new XeroInvalidConfigurationException(self::$exceptionMessage);
  }

  /**
   * {@inheritdoc}
   */
  public function isValidPrivateKey($filename): bool {
    throw new XeroInvalidConfigurationException(self::$exceptionMessage);
  }

  /**
   * {@inheritdoc}
   */
  public static function getRequestToken($consumer_key, $consumer_secret, $options = []): array {
    throw new XeroInvalidConfigurationException(self::$exceptionMessage);
  }

  /**
   * {@inheritdoc}
   */
  public static function getAccessToken($consumer_key, $consumer_secret, $token, $token_secret, $verifier, $options = []): array {
    throw new XeroInvalidConfigurationException(self::$exceptionMessage);
  }

  /**
   * {@inheritdoc}
   */
  public function getConnections(): array {
    throw new XeroInvalidConfigurationException(self::$exceptionMessage);
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromToken($id, $secret, $token, $grant = NULL, $api = 'accounting', array $options = [], array $collaborators = [], $redirectUri = ''): static {
    return new static([]);
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromConfig(array $config, array $options = []): static {
    throw new XeroInvalidConfigurationException(self::$exceptionMessage);
  }

  /**
   * {@inheritdoc}
   */
  public function get(UriInterface|string $uri = '', array $options = []): ResponseInterface {
    throw new XeroInvalidConfigurationException(self::$exceptionMessage);
  }

  /**
   * {@inheritdoc}
   */
  public function post(UriInterface|string $uri = '', array $options = []): ResponseInterface {
    throw new XeroInvalidConfigurationException(self::$exceptionMessage);
  }

  /**
   * {@inheritdoc}
   */
  public function put(UriInterface|string $uri = '', array $options = []): ResponseInterface {
    throw new XeroInvalidConfigurationException(self::$exceptionMessage);
  }

  /**
   * {@inheritdoc}
   */
  public function getTenantIds(): array {
    return [];
  }

}
