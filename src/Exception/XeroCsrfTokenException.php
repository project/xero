<?php

namespace Drupal\xero\Exception;

/**
 * Thrown when a CSRF token does not match or one is already stored.
 */
class XeroCsrfTokenException extends \Exception {}
