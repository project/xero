<?php

namespace Drupal\xero\Exception;

/**
 * Exception thrown when invalid configuration detected.
 *
 * This should only be used for OAuth2 related configuration issues rather than
 * Guzzle request exceptions.
 *
 * @see \Radcliffe\Xero\Exception\XeroRequestException
 */
class XeroInvalidConfigurationException extends \Exception {}
