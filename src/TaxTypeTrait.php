<?php

namespace Drupal\xero;

/**
 * Provides tax types in Xero.
 */
trait TaxTypeTrait {

  /**
   * Provide the correct Xero Tax Types for validation.
   *
   * @return string[]
   *   A list of tax type enumerated strings.
   */
  public static function getTaxTypes(): array {
    return [
      // Global types.
      'INPUT', 'OUTPUT', 'NONE', 'GSTONIMPORTS',
      // Australia.
      'CAPEXINPUT', 'EXEMPTEXPORT', 'EXEMPTEXPENSES', 'EXEMPTCAPITAL',
      'EXEMPTOUTPUT', 'INPUTTAXED', 'BASEXCLUDED', 'GSTONCAPIMPORTS',
      'GSTONIMPORTS',
      // New Zealand.
      'INPUT2', 'OUTPUT2', 'ZERORATED',
      // UK.
      'CAPEXINPUT2', 'CAPEXOUTPUT', 'CAPEXOUTPUT2', 'CAPEXSRINPUT',
      'CAPEXSROUTPUT', 'ECZRINPUT', 'ECZROUTPUT', 'ECZROUTPUTSERVICES',
      'EXEMPTINPUT', 'RRINPUT', 'RROUTPUT', 'SRINPUT', 'SROUTPUT',
      'ZERORATEDINPUT', 'ZERORATEDOUTPUT',
    ];
  }

}
