<?php

namespace Drupal\xero\Plugin\DataType;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\Attribute\DataType;
use Drupal\xero\TypedData\Definition\OrganisationDefinition;

/**
 * Describes an organisation that uses Xero.
 *
 * @DataType(
 *   id = "xero_organisation",
 *   label = @Translation("Xero Organization"),
 *   definition_class = "\Drupal\xero\TypedData\Definition\OrganisationDefinition",
 *   list_class = "\Drupal\xero\Plugin\DataType\XeroItemList"
 * )
 */
#[DataType(
  id: 'xero_organisation',
  label: new TranslatableMarkup('Xero Organization'),
  definition_class: OrganisationDefinition::class,
  list_class: XeroItemList::class,
)]
class Organisation extends XeroComplexItemBase {

  /**
   * {@inheritdoc}
   */
  static public $guid_name;

  /**
   * {@inheritdoc}
   */
  static public $xero_name = 'Organisations';

  /**
   * {@inheritdoc}
   */
  static public $plural_name = 'Organisation';

  /**
   * {@inheritdoc}
   */
  static public $label = 'Name';

}
