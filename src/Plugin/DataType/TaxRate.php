<?php

namespace Drupal\xero\Plugin\DataType;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\Attribute\DataType;
use Drupal\xero\TypedData\Definition\TaxRateDefinition;

/**
 * Describes a Xero tax rate.
 *
 * @DataType(
 *   id = "xero_tax_rate",
 *   label = @Translation("Xero Tax Rate"),
 *   definition_class = "\Drupal\xero\TypedData\Definition\TaxRateDefinition",
 *   list_class = "\Drupal\xero\Plugin\DataType\XeroItemList"
 * )
 */
#[DataType(
  id: 'xero_tax_rate',
  label: new TranslatableMarkup('Xero Tax Rate'),
  definition_class: TaxRateDefinition::class,
  list_class: XeroItemList::class,
)]
class TaxRate extends XeroComplexItemBase {

  /**
   * {@inheritdoc}
   */
  static public $guid_name;

  /**
   * {@inheritdoc}
   */
  static public $xero_name = 'TaxRate';

  /**
   * {@inheritdoc}
   */
  static public $plural_name = 'TaxRates';

  /**
   * {@inheritdoc}
   */
  static public $label = 'Name';

}
