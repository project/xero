<?php

namespace Drupal\xero\Plugin\DataType;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\Attribute\DataType;
use Drupal\xero\TypedData\Definition\TrackingCategoryOptionDefinition;

/**
 * Tracking Category option.
 *
 * @DataType(
 *   id = "xero_tracking_category_option",
 *   label = @Translation("Tracking Category Option"),
 *   definition_class = "\Drupal\xero\TypedData\Definition\TrackingCategoryOptionDefinition",
 *   list_class = "\Drupal\xero\Plugin\DataType\XeroItemList"
 * )
 */
#[DataType(
  id: 'xero_tracking_category_option',
  label: new TranslatableMarkup('Tracking Category Option'),
  definition_class: TrackingCategoryOptionDefinition::class,
  list_class: XeroItemList::class,
)]
class TrackingCategoryOption extends XeroComplexItemBase {

  /**
   * {@inheritdoc}
   */
  static public $guid_name = 'TrackingOptionID';

  /**
   * {@inheritdoc}
   */
  static public $xero_name = 'Option';

  /**
   * {@inheritdoc}
   */
  static public $plural_name = 'Options';

  /**
   * {@inheritdoc}
   */
  static public $label = 'Name';

}
