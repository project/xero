<?php

namespace Drupal\xero\Plugin\DataType;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\Attribute\DataType;
use Drupal\xero\TypedData\Definition\BankTransferDefinition;

/**
 * Describes a Xero bank transfer.
 *
 * @DataType(
 *   id = "xero_bank_transfer",
 *   label = @Translation("Xero Bank Transfer"),
 *   definition_class = "\Drupal\xero\TypedData\Definition\BankTransferDefinition",
 *   list_class = "\Drupal\xero\Plugin\DataType\XeroItemList"
 * )
 */
#[DataType(
  id: 'xero_bank_transfer',
  label: new TranslatableMarkup('Xero Bank Transfer'),
  definition_class: BankTransferDefinition::class,
  list_class: XeroItemList::class,
)]
class BankTransfer extends XeroComplexItemBase {

  /**
   * {@inheritdoc}
   */
  static public $guid_name = 'BankTransferID';

  /**
   * {@inheritdoc}
   */
  static public $xero_name = 'BankTransfer';

  /**
   * {@inheritdoc}
   */
  static public $plural_name = 'BankTransfers';

  /**
   * {@inheritdoc}
   */
  static public $label = 'Amount';

}
