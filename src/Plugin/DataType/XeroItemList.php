<?php

namespace Drupal\xero\Plugin\DataType;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\Attribute\DataType;
use Drupal\Core\TypedData\ListDataDefinition;
use Drupal\Core\TypedData\Plugin\DataType\ItemList;

/**
 * Represents an ordered list of Xero type items.
 *
 * @DataType(
 *   id = "xero_list",
 *   label = @Translation("Xero list"),
 *   definition_class = "\Drupal\Core\TypedData\ListDataDefinition"
 * )
 */
#[DataType(
  id: 'xero_item_list',
  label: new TranslatableMarkup('Xero list'),
  definition_class: ListDataDefinition::class,
)]
class XeroItemList extends ItemList {}
