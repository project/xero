<?php

namespace Drupal\xero\Plugin\DataType;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\Attribute\DataType;
use Drupal\Core\Url;
use Drupal\xero\TypedData\Definition\LinkDefinition;

/**
 * Xero link.
 *
 * @DataType(
 *   id = "xero_link",
 *   label = @Translation("Xero Link"),
 *   definition_class = "\Drupal\xero\TypedData\Definition\LinkDefinition",
 *   list_class = "\Drupal\xero\Plugin\DataType\XeroItemList"
 * )
 */
#[DataType(
  id: 'xero_link',
  label: new TranslatableMarkup('Xero Link'),
  definition_class: LinkDefinition::class,
  list_class: XeroItemList::class,
)]
class Link extends XeroItemBase {

  /**
   * {@inheritdoc}
   */
  static public $xero_name = 'Link';

  /**
   * {@inheritdoc}
   */
  static public $plural_name = 'Links';

  /**
   * {@inheritdoc}
   */
  static public $label = 'Description';

  /**
   * {@inheritdoc}
   */
  public function view(): array {
    return [
      '#type' => 'link',
      '#title' => $this->get('Description')->getValue(),
      '#url' => Url::fromUri($this->get('Url')->getValue(), ['absolute' => TRUE]),
    ];
  }

}
