<?php

namespace Drupal\xero\Plugin\DataType;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\Attribute\DataType;
use Drupal\xero\TypedData\Definition\LinkedTransactionDefinition;

/**
 * Describes Xero linked transaction data type.
 *
 * @DataType(
 *   id = "xero_linked_transaction",
 *   label = @Translation("Xero Linked Transaction"),
 *   definition_class = "\Drupal\xero\TypedData\Definition\LinkedTransactionDefinition",
 *   list_class = "\Drupal\xero\Plugin\DataType\XeroItemList"
 * )
 */
#[DataType(
  id: 'xero_linked_transaction',
  label: new TranslatableMarkup('Xero Linked Transaction'),
  definition_class: LinkedTransactionDefinition::class,
  list_class: XeroItemList::class,
)]
class LinkedTransaction extends XeroComplexItemBase {

  /**
   * {@inheritdoc}
   */
  static public $guid_name = 'LinkedTransactionID';

  /**
   * {@inheritdoc}
   */
  static public $xero_name = 'LinkedTtransaction';

  /**
   * {@inheritdoc}
   */
  static public $plural_name = 'LinkedTransactions';

  /**
   * {@inheritdoc}
   */
  static public $label = 'SourceTransactionID';

}
