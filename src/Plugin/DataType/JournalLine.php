<?php

namespace Drupal\xero\Plugin\DataType;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\Attribute\DataType;
use Drupal\xero\TypedData\Definition\JournalLineDefinition;

/**
 * Xero journal line item type.
 *
 * @DataType(
 *   id = "xero_journal_line",
 *   label = @Translation("Xero Journal Line"),
 *   definition_class = "\Drupal\xero\TypedData\Definition\JournalLineDefinition",
 *   list_class = "\Drupal\xero\Plugin\DataType\XeroItemList"
 * )
 */
#[DataType(
  id: 'xero_journal_line',
  label: new TranslatableMarkup('Xero Journal Line'),
  definition_class: JournalLineDefinition::class,
  list_class: XeroItemList::class,
)]
class JournalLine extends XeroItemBase {

  /**
   * {@inheritdoc}
   */
  static public $xero_name = 'JournalLine';

  /**
   * {@inheritdoc}
   */
  static public $plural_name = 'JournalLines';

  /**
   * {@inheritdoc}
   */
  static public $label = 'JournalLineID';

  /**
   * {@inheritdoc}
   */
  public function view(): array {
    // Expectation is that line items are rendered by their parent.
    return [];
  }

}
