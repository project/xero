<?php

namespace Drupal\xero\Plugin\DataType;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\Attribute\DataType;
use Drupal\xero\TypedData\Definition\CreditDefinition;

/**
 * Xero CreditNote type.
 *
 * @DataType(
 *   id = "xero_credit_note",
 *   label = @Translation("Xero Credit Note"),
 *   definition_class = "\Drupal\xero\TypedData\Definition\CreditDefinition",
 *   list_class = "\Drupal\xero\Plugin\DataType\XeroItemList"
 * )
 */
#[DataType(
  id: 'xero_credit_note',
  label: new TranslatableMarkup('Xero Credit Note'),
  definition_class: CreditDefinition::class,
  list_class: XeroItemList::class,
)]
class CreditNote extends XeroComplexItemBase {

  /**
   * {@inheritdoc}
   */
  static public $guid_name = 'CreditNoteID';

  /**
   * {@inheritdoc}
   */
  static public $plural_name = 'CreditNotes';

  /**
   * {@inheritdoc}
   */
  static public $label = 'CreditNoteNumber';

  /**
   * {@inheritdoc}
   */
  static public $xero_name = 'CreditNote';

  /**
   * {@inheritdoc}
   */
  public function view(): array {
    $header = [
      $this->t('Description'),
      $this->t('Quantity'),
      $this->t('Unit amount'),
      $this->t('Account code'),
      $this->t('Line amount'),
    ];
    $className = substr($this->getPluginId(), 5);
    $rows = [];

    $build = [
      '#theme' => $this->getPluginId(),
      '#credit' => $this->getValue(),
      '#attributes' => [
        'class' => ['xero-item', 'xero-item--' . $className],
      ],
      '#contact' => $this->get('Contact')->view(),
      '#items' => [
        '#theme' => 'table',
        '#header' => $header,
      ],
    ];

    /** @var \Drupal\Core\TypedData\ListInterface $lineitems */
    $lineitems = $this->get('LineItems');
    /** @var \Drupal\xero\Plugin\DataType\LineItem $lineitem */
    foreach ($lineitems as $lineitem) {
      $row = [];

      $row[] = $lineitem->get('Description')->getString();
      $row[] = $lineitem->get('Quantity')->getString();
      $row[] = $lineitem->get('UnitAmount')->getString();
      $row[] = $lineitem->get('AccountCode')->getString();
      $row[] = $lineitem->get('LineAmount')->getString();

      $rows[] = $row;
    }

    $rows[] = ['', '', '', $this->t('Sub-Total'), $this->get('SubTotal')->getString()];
    $rows[] = ['', '', '', $this->t('Tax'), $this->get('TotalTax')->getString()];
    $rows[] = ['', '', '', $this->t('Total'), $this->get('Total')->getString()];

    $build['#items']['rows'] = $rows;

    return $build;
  }

}
