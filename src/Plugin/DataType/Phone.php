<?php

namespace Drupal\xero\Plugin\DataType;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\Attribute\DataType;
use Drupal\Core\TypedData\Exception\MissingDataException;
use Drupal\xero\TypedData\Definition\PhoneDefinition;

/**
 * Xero phone type.
 *
 * @DataType(
 *   id = "xero_phone",
 *   label = @Translation("Xero Phone"),
 *   definition_class = "\Drupal\xero\TypedData\Definition\PhoneDefinition",
 *   list_class = "\Drupal\xero\Plugin\DataType\XeroItemList"
 * )
 */
#[DataType(
  id: 'xero_phone',
  label: new TranslatableMarkup('Xero Phone'),
  definition_class: PhoneDefinition::class,
  list_class: XeroItemList::class,
)]
class Phone extends XeroItemBase {

  /**
   * {@inheritdoc}
   */
  static public $xero_name = 'Phone';

  /**
   * {@inheritdoc}
   */
  static public $plural_name = 'Phones';

  /**
   * {@inheritdoc}
   */
  static public $label = 'PhoneNumber';

  /**
   * Get the canonical phone number.
   *
   * @return string
   *   The full phone number.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function getPhone(): string {
    return $this->get('PhoneCountryCode')->getValue() . '-' . $this->get('PhoneAreaCode')->getValue() . '-' . $this->get('PhoneNumber')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function view(): array {
    try {
      $phone = $this->getPhone();
    }
    catch (MissingDataException $e) {
      $phone = '';
    }

    return [
      '#type' => 'html_tag',
      '#tag' => 'span',
      '#value' => $phone,
    ];
  }

}
