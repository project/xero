<?php

namespace Drupal\xero\Plugin\DataType;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\Attribute\DataType;
use Drupal\xero\TypedData\Definition\CurrencyDefinition;

/**
 * Describes a Xero currency data type.
 *
 * @DataType(
 *   id = "xero_currency",
 *   label = @Translation("Xero Currency"),
 *   definition_class = "\Drupal\xero\TypedData\Definition\CurrencyDefinition",
 *   list_class = "\Drupal\xero\Plugin\DataType\XeroItemList"
 * )
 */
#[DataType(
  id: 'xero_currency',
  label: new TranslatableMarkup('Xero Currency'),
  definition_class: CurrencyDefinition::class,
  list_class: XeroItemList::class,
)]
class Currency extends XeroComplexItemBase {

  /**
   * {@inheritdoc}
   */
  static public $guid_name;

  /**
   * {@inheritdoc}
   */
  static public $xero_name = 'Currency';

  /**
   * {@inheritdoc}
   */
  static public $plural_name = 'Currencies';

  /**
   * {@inheritdoc}
   */
  static public $label = 'Code';

}
