<?php

namespace Drupal\xero\Plugin\DataType;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\Attribute\DataType;
use Drupal\xero\TypedData\Definition\DetailDefinition;

/**
 * Xero detail item type.
 *
 * @DataType(
 *   id = "xero_detail",
 *   label = @Translation("Xero Detail"),
 *   definition_class = "\Drupal\xero\TypedData\Definition\DetailDefinition",
 *   list_class = "\Drupal\xero\Plugin\DataType\XeroItemList"
 * )
 */
#[DataType(
  id: 'xero_detail',
  label: new TranslatableMarkup('Xero Detail'),
  definition_class: DetailDefinition::class,
  list_class: XeroItemList::class,
)]
class Detail extends XeroItemBase {

  /**
   * {@inheritdoc}
   */
  public static $plural_name = '';

  /**
   * {@inheritdoc}
   */
  public static $xero_name = 'Detail';

  /**
   * {@inheritdoc}
   */
  public static $label = 'UnitPrice';

  /**
   * {@inheritdoc}
   */
  public static function getXeroProperty($name): string {
    $allowed_props = ['label'];
    if (!in_array($name, $allowed_props)) {
      throw new \InvalidArgumentException('Invalid xero property.');
    }

    return static::${$name};
  }

}
