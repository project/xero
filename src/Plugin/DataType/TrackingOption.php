<?php

namespace Drupal\xero\Plugin\DataType;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\Attribute\DataType;
use Drupal\xero\TypedData\Definition\TrackingOptionDefinition;

/**
 * Xero Tracking Option type for BankTransaction line items.
 *
 * @see https://developer.xero.com/documentation/api/banktransactions
 *
 * @DataType(
 *   id = "xero_tracking_option",
 *   label = @Translation("Xero Tracking Option"),
 *   definition_class = "\Drupal\xero\TypedData\Definition\TrackingOptionDefinition",
 *   list_class = "\Drupal\xero\Plugin\DataType\XeroItemList"
 * )
 */
#[DataType(
  id: 'xero_tracking_option',
  label: new TranslatableMarkup('Xero Tracking Option'),
  definition_class: TrackingOptionDefinition::class,
  list_class: XeroItemList::class,
)]
class TrackingOption extends XeroItemBase {

  /**
   * {@inheritdoc}
   */
  public static $xero_name = 'TrackingCategory';

  /**
   * {@inheritdoc}
   */
  public static $plural_name = 'Tracking';

  /**
   * {@inheritdoc}
   */
  public static $label = 'Option';

  /**
   * {@inheritdoc}
   */
  public function view(): array {
    $option = $this->getOption();
    $name = $this->getNameValue();

    return [
      '#markup' => $option . ': ' . $name,
    ];
  }

  /**
   * Gets the option value.
   *
   * @return string
   *   The value to return.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function getOption(): string {
    $option = $this->get('Option');
    return $option->getValue() ? $option->getValue() : 'N/A';
  }

  /**
   * Gets the name value.
   *
   * @return string
   *   The value to return.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function getNameValue(): string {
    $name = $this->get('Name');
    return $name->getValue() ? $name->getValue() : 'N/A';
  }

}
