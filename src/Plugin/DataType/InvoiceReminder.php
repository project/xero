<?php

namespace Drupal\xero\Plugin\DataType;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\Attribute\DataType;
use Drupal\xero\TypedData\Definition\InvoiceReminderDefinition;

/**
 * Describes Xero invoice reminder data type.
 *
 * @DataType(
 *   id = "xero_invoice_reminder",
 *   label = @Translation("Xero Invoice Reminder"),
 *   definition_class = "\Drupal\xero\TypedData\Definition\InvoiceReminderDefinition",
 *   list_class = "\Drupal\xero\Plugin\DataType\XeroItemList"
 * )
 */
#[DataType(
  id: 'xero_invoice_reminder',
  label: new TranslatableMarkup('Xero Invoice Reminder'),
  definition_class: InvoiceReminderDefinition::class,
  list_class: XeroItemList::class,
)]
class InvoiceReminder extends XeroComplexItemBase {

  /**
   * {@inheritdoc}
   */
  static public $guid_name;

  /**
   * {@inheritdoc}
   */
  static public $xero_name = 'InvoiceReminder';

  /**
   * {@inheritdoc}
   */
  static public $plural_name = 'InvoiceReminders';

  /**
   * {@inheritdoc}
   */
  static public $label = 'Enabled';

}
