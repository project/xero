<?php

namespace Drupal\xero\Plugin\DataType;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\Attribute\DataType;
use Drupal\xero\TypedData\Definition\TrackingCategoryDefinition;

/**
 * Xero Tracking Category type.
 *
 * @see https://developer.xero.com/documentation/api/tracking-categories
 *
 * @DataType(
 *   id = "xero_tracking",
 *   label = @Translation("Xero Tracking Category"),
 *   definition_class = "\Drupal\xero\TypedData\Definition\TrackingCategoryDefinition",
 *   list_class = "\Drupal\xero\Plugin\DataType\XeroItemList",
 * )
 */
#[DataType(
  id: 'xero_tracking',
  label: new TranslatableMarkup('Xero Tracking Category'),
  definition_class: TrackingCategoryDefinition::class,
  list_class: XeroItemList::class,
)]
class TrackingCategory extends XeroComplexItemBase {

  /**
   * {@inheritdoc}
   */
  static public $guid_name = 'TrackingCategoryID';

  /**
   * {@inheritdoc}
   */
  static public $xero_name = 'TrackingCategory';

  /**
   * {@inheritdoc}
   */
  static public $plural_name = 'TrackingCategories';

  /**
   * {@inheritdoc}
   */
  static public $label = 'Name';

}
