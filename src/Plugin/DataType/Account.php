<?php

namespace Drupal\xero\Plugin\DataType;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\Attribute\DataType;
use Drupal\xero\TypedData\Definition\AccountDefinition;

/**
 * Xero Account type.
 *
 * @DataType(
 *   id = "xero_account",
 *   label = @Translation("Xero Account"),
 *   definition_class = "\Drupal\xero\TypedData\Definition\AccountDefinition",
 *   list_class = "\Drupal\xero\Plugin\DataType\XeroItemList"
 * )
 */
#[DataType(
  id: 'xero_account',
  label: new TranslatableMarkup('Xero Account'),
  definition_class: AccountDefinition::class,
  list_class: XeroItemList::class,
)]
class Account extends XeroComplexItemBase {

  /**
   * {@inheritdoc}
   */
  static public $guid_name = 'AccountID';

  /**
   * {@inheritdoc}
   */
  static public $xero_name = 'Account';

  /**
   * {@inheritdoc}
   */
  static public $plural_name = 'Accounts';

  /**
   * {@inheritdoc}
   */
  static public $label = 'Name';

  /**
   * See if an account can be used as a revenue account.
   *
   * @return bool
   *   Return TRUE if the account is revenue-based.
   *
   * @throws \Exception
   */
  public function isRevenue(): bool {
    $class = $this->get('Class')->getValue();
    if ($class && $class == 'REVENUE') {
      return TRUE;
    }
    elseif (!$class) {
      throw new \Exception('Invalid use of isRevenue method');
    }

    return FALSE;
  }

  /**
   * See if an account is a bank account.
   *
   * @retun bool
   *   Return TRUE if the account is a bank account.
   */
  public function isBankAccount(): bool {
    $type = $this->get('Type')->getValue();

    return $type == 'BANK';
  }

}
