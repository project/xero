<?php

namespace Drupal\xero\Plugin\DataType;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\Attribute\DataType;
use Drupal\xero\TypedData\Definition\BrandingThemeDefinition;

/**
 * Describes a Xero branding theme.
 *
 * @DataType(
 *   id = "xero_branding_theme",
 *   label = @Translation("Xero Branding Theme"),
 *   definition_class = "\Drupal\xero\TypedData\Definition\BrandingThemeDefinition",
 *   list_class = "\Drupal\xero\Plugin\DataType\XeroItemList"
 * )
 */
#[DataType(
  id: 'xero_branding_theme',
  label: new TranslatableMarkup('Xero Branding Theme'),
  definition_class: BrandingThemeDefinition::class,
  list_class: XeroItemList::class,
)]
class BrandingTheme extends XeroComplexItemBase {

  /**
   * {@inheritdoc}
   */
  static public $guid_name = 'BrandingThemeID';

  /**
   * {@inheritdoc}
   */
  static public $xero_name = 'BrandingTheme';

  /**
   * {@inheritdoc}
   */
  static public $plural_name = 'BrandingThemes';

  /**
   * {@inheritdoc}
   */
  static public $label = 'Name';

}
