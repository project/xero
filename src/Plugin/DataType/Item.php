<?php

namespace Drupal\xero\Plugin\DataType;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\Attribute\DataType;
use Drupal\xero\TypedData\Definition\ItemDefinition;

/**
 * Xero Item type.
 *
 * @DataType(
 *   id = "xero_item",
 *   label = @Translation("Xero Item"),
 *   definition_class = "\Drupal\xero\TypedData\Definition\ItemDefinition",
 *   list_class = "\Drupal\xero\Plugin\DataType\XeroItemList"
 * )
 */
#[DataType(
  id: 'xero_item',
  label: new TranslatableMarkup('Xero Item'),
  definition_class: ItemDefinition::class,
  list_class: XeroItemList::class,
)]
class Item extends XeroComplexItemBase {

  /**
   * {@inheritdoc}
   */
  static public $guid_name = 'Code';

  /**
   * {@inheritdoc}
   */
  static public $xero_name = 'Item';

  /**
   * {@inheritdoc}
   */
  static public $plural_name = 'Items';

  /**
   * {@inheritdoc}
   */
  static public $label = 'Code';

}
