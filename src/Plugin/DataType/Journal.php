<?php

namespace Drupal\xero\Plugin\DataType;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\Attribute\DataType;
use Drupal\xero\TypedData\Definition\JournalDefinition;

/**
 * Xero Journal type.
 *
 * @DataType(
 *   id = "xero_journal",
 *   label = @Translation("Xero Journal"),
 *   definition_class = "\Drupal\xero\TypedData\Definition\JournalDefinition",
 *   list_class = "\Drupal\xero\Plugin\DataType\XeroItemList"
 * )
 */
#[DataType(
  id: 'xero_journal',
  label: new TranslatableMarkup('Xero Journal'),
  definition_class: JournalDefinition::class,
  list_class: XeroItemList::class,
)]
class Journal extends XeroComplexItemBase {

  /**
   * {@inheritdoc}
   */
  static public $guid_name = 'JournalID';

  /**
   * {@inheritdoc}
   */
  static public $xero_name = 'Journal';

  /**
   * {@inheritdoc}
   */
  static public $plural_name = 'Journals';

  /**
   * {@inheritdoc}
   */
  static public $label = 'JournalNumber';

  /**
   * {@inheritdoc}
   */
  public function view(): array {
    $header = [
      $this->t('Code'),
      $this->t('Type'),
      $this->t('Name'),
      $this->t('Description'),
      $this->t('NetAmount'),
      $this->t('GrossAmount'),
      $this->t('TaxAmount'),
      $this->t('TaxType'),
      $this->t('TaxName'),
    ];
    $rows = [];
    $className = substr($this->getPluginId(), 5);

    $item = [
      '#theme' => $this->getPluginId(),
      '#journal' => $this->getValue(),
      '#items' => [
        '#theme' => 'table',
        '#header' => $header,
      ],
      '#attributes' => [
        'class' => ['xero-item', 'xero-item--' . $className],
      ],
    ];

    /** @var \Drupal\Core\TypedData\ListInterface $journal */
    $journals = $this->get('JournalLines');
    /** @var \Drupal\xero\Plugin\DataType\JournalLine $journal */
    foreach ($journals as $journal) {
      $rows[] = [
        $journal->get('AccountCode')->getString(),
        $journal->get('AccountType')->getString(),
        $journal->get('AccountName')->getString(),
        $journal->get('Description')->getString(),
        $journal->get('NetAmount')->getString(),
        $journal->get('GrossAmount')->getString(),
        $journal->get('TaxAmount')->getString(),
        $journal->get('TaxType')->getString(),
        $journal->get('TaxName')->getString(),
      ];
    }

    $item['#rows'] = $rows;

    return $item;
  }

}
