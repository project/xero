<?php

namespace Drupal\xero\Plugin\DataType;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\Attribute\DataType;
use Drupal\xero\TypedData\Definition\TaxComponentDefinition;

/**
 * Describes the Xero tax component.
 *
 * @DataType(
 *   id = "xero_tax_component",
 *   label = @Translation("Xero Tax Component"),
 *   definition_class = "\Drupal\xero\TypedData\Definition\TaxComponentDefinition",
 *   list_class = "\Drupal\xero\Plugin\DataType\XeroItemList"
 * )
 */
#[DataType(
  id: 'xero_tax_component',
  label: new TranslatableMarkup('Xero Tax Component'),
  definition_class: TaxComponentDefinition::class,
  list_class: XeroItemList::class,
)]
class TaxComponent extends XeroItemBase {

  /**
   * {@inheritdoc}
   */
  public static $xero_name = 'TaxComponent';

  /**
   * {@inheritdoc}
   */
  public static $plural_name = 'TaxComponents';

  /**
   * {@inheritdoc}
   */
  public static $label = 'Name';

}
