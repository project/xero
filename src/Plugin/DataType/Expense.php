<?php

namespace Drupal\xero\Plugin\DataType;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\Attribute\DataType;
use Drupal\xero\TypedData\Definition\ExpenseDefinition;

/**
 * Xero Expense type.
 *
 * Expense claims are deprecated by Xero as of 2018.10. Only Xero customers who
 * used expenes claims between 2018.01 - 2018.07 may continue to use this API.
 *
 * @see https://developer.xero.com/documentation/api/accounting/expenseclaims
 *
 * @DataType(
 *   id = "xero_expense",
 *   label = @Translation("Xero Expense Claim"),
 *   definition_class = "\Drupal\xero\TypedData\Definition\ExpenseDefinition",
 *   list_class = "\Drupal\xero\Plugin\DataType\XeroItemList"
 * )
 */
#[DataType(
  id: 'xero_expense',
  label: new TranslatableMarkup('Xero Expense Claim'),
  definition_class: ExpenseDefinition::class,
  list_class: XeroItemList::class,
)]
class Expense extends XeroComplexItemBase {

  /**
   * {@inheritdoc}
   */
  static public $guid_name = 'ExpenseClaimID';

  /**
   * {@inheritdoc}
   */
  static public $xero_name = 'ExpenseClaim';

  /**
   * {@inheritdoc}
   */
  static public $plural_name = 'ExpenseClaims';

  /**
   * {@inheritdoc}
   */
  static public $label = 'ExpenseClaimID';

  /**
   * {@inheritdoc}
   */
  public function view(): array {
    $payment_rows = [];
    $payment_header = [
      $this->t('Type'),
      $this->t('Status'),
      $this->t('Amount'),
      $this->t('Date'),
    ];
    $className = substr($this->getPluginId(), 5);

    $build = [
      '#theme' => $this->getPluginId(),
      '#expense' => $this->getValue(),
      '#user' => $this->get('User')->view(),
      '#receipts' => [
        '#type' => 'container',
      ],
      '#payments' => [
        '#theme' => 'table',
        '#header' => $payment_header,
      ],
      '#attributes' => [
        'class' => ['xero-item', 'xero-item--' . $className],
      ],
    ];

    /** @var \Drupal\Core\TypedData\ListInterface $receipts */
    $receipts = $this->get('Receipts');
    /** @var \Drupal\xero\Plugin\DataType\Receipt $receipt */
    foreach ($receipts as $receipt) {
      $build['#recepts'][] = $receipt->view();
    }

    /** @var \Drupal\Core\TypedData\ListInterface $payments */
    $payments = $this->get('Payments');
    /** @var \Drupal\xero\Plugin\DataType\Payment $payment */
    foreach ($payments as $payment) {
      $payment_rows[] = [
        $payment->get('PaymentType')->getString(),
        $payment->get('Status')->getString(),
        $payment->get('Amount')->getString(),
        $payment->get('Date')->getString(),
      ];
    }

    $build['#payments']['#rows'] = $payment_rows;

    return $build;
  }

}
