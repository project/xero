<?php

namespace Drupal\xero\Plugin\DataType;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\Attribute\DataType;
use Drupal\xero\TypedData\Definition\ContactDefinition;

/**
 * Xero Contact type.
 *
 * @DataType(
 *   id = "xero_contact",
 *   label = @Translation("Xero Contact"),
 *   definition_class = "\Drupal\xero\TypedData\Definition\ContactDefinition",
 *   list_class = "\Drupal\xero\Plugin\DataType\XeroItemList"
 * )
 */
#[DataType(
  id: 'xero_contact',
  label: new TranslatableMarkup('Xero Contact'),
  definition_class: ContactDefinition::class,
  list_class: XeroItemList::class,
)]
class Contact extends XeroComplexItemBase {

  /**
   * {@inheritdoc}
   */
  static public $guid_name = 'ContactID';

  /**
   * {@inheritdoc}
   */
  static public $xero_name = 'Contact';

  /**
   * {@inheritdoc}
   */
  static public $plural_name = 'Contacts';

  /**
   * {@inheritdoc}
   */
  static public $label = 'Name';

  /**
   * {@inheritdoc}
   */
  public function get($property_name) {
    if ($property_name === 'Status') {
      @trigger_error('The Status property is deprecated in xero:3.0.0 and will be removed in xero:4.0.0. This property was changed in Xero API. See https://developer.xero.com/documentation/api/accounting/contacts#get-contacts', E_USER_DEPRECATED);
      return parent::get('ContactStatus');
    }
    return parent::get($property_name);
  }

  /**
   * Determines if the contact is a customer.
   *
   * The value returned and set by the API is a string, but hopefully that is
   * normalized by Serializer as a boolean.
   *
   * @return bool
   *   TRUE if the contact is a customer.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function isCustomer(): bool {
    $isCustomer = $this->get('IsCustomer')->getValue();

    return $isCustomer == TRUE;
  }

  /**
   * Determines if the contact is a supplier.
   *
   * @return bool
   *   TRUE if the contact is a supplier.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function isSupplier(): bool {
    $isSupplier = $this->get('IsSupplier')->getValue();

    return $isSupplier == TRUE;
  }

}
