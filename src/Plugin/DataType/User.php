<?php

namespace Drupal\xero\Plugin\DataType;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\Attribute\DataType;
use Drupal\xero\TypedData\Definition\UserDefinition;

/**
 * Xero User type.
 *
 * @DataType(
 *   id = "xero_user",
 *   label = @Translation("Xero User"),
 *   definition_class = "\Drupal\xero\TypedData\Definition\UserDefinition",
 *   list_class = "\Drupal\xero\Plugin\DataType\XeroItemList"
 * )
 */
#[DataType(
  id: 'xero_user',
  label: new TranslatableMarkup('Xero User'),
  definition_class: UserDefinition::class,
  list_class: XeroItemList::class,
)]
class User extends XeroComplexItemBase {

  /**
   * {@inheritdoc}
   */
  static public $guid_name = 'UserID';

  /**
   * {@inheritdoc}
   */
  static public $xero_name = 'User';

  /**
   * {@inheritdoc}
   */
  static public $plural_name = 'Users';

  /**
   * {@inheritdoc}
   */
  static public $label = 'EmailAddress';

  /**
   * Is the user account a subscriber?
   *
   * @return bool
   *   Return TRUE if the user is a subscriber.
   */
  public function isSubscriber(): bool {
    return $this->get('IsSubscriber')->getValue();
  }

}
