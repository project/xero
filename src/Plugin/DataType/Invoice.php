<?php

namespace Drupal\xero\Plugin\DataType;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\Attribute\DataType;
use Drupal\xero\TypedData\Definition\InvoiceDefinition;

/**
 * Xero Invoice type.
 *
 * @DataType(
 *   id = "xero_invoice",
 *   label = @Translation("Xero Invoice"),
 *   definition_class = "\Drupal\xero\TypedData\Definition\InvoiceDefinition",
 *   list_class = "\Drupal\xero\Plugin\DataType\XeroItemList"
 * )
 */
#[DataType(
  id: 'xero_invoice',
  label: new TranslatableMarkup('Xero Invoice'),
  definition_class: InvoiceDefinition::class,
  list_class: XeroItemList::class,
)]
class Invoice extends XeroComplexItemBase {

  /**
   * {@inheritdoc}
   */
  static public $guid_name = 'InvoiceID';

  /**
   * {@inheritdoc}
   */
  static public $xero_name = "Invoice";

  /**
   * {@inheritdoc}
   */
  static public $plural_name = 'Invoices';

  /**
   * {@inheritdoc}
   */
  static public $label = 'InvoiceNumber';

  /**
   * {@inheritdoc}
   */
  public function view(): array {
    $header = [
      $this->t('Description'),
      $this->t('Quantity'),
      $this->t('Unit amount'),
      $this->t('Account code'),
      $this->t('Tax type'),
      $this->t('Tax amount'),
      $this->t('Line amount'),
    ];
    $payment_header = [
      $this->t('Type'),
      $this->t('Status'),
      $this->t('Amount'),
      $this->t('Date'),
    ];
    $className = substr($this->getPluginId(), 5);
    $rows = [];

    /** @var \Drupal\xero\TypedData\XeroComplexItemInterface|null $contact */
    $contact = $this->get('Contact');

    $build = [
      '#theme' => $this->getPluginId(),
      '#invoice' => $this->getValue(),
      '#attributes' => [
        'class' => ['xero-item', 'xero-item--' . $className],
      ],
      '#contact' => $contact ? $contact->view() : '',
      '#items' => [
        '#theme' => 'table',
        '#header' => $header,
      ],
      '#payments' => [
        '#theme' => 'table',
        '#header' => $payment_header,
      ],
    ];

    /** @var \Drupal\Core\TypedData\ListInterface $lineitems */
    $lineitems = $this->get('LineItems');
    /** @var \Drupal\xero\Plugin\DataType\LineItem $lineitem */
    foreach ($lineitems as $lineitem) {
      $rows[] = [
        $lineitem->get('Description')->getString(),
        $lineitem->get('Quantity')->getString(),
        $lineitem->get('UnitAmount')->getString(),
        $lineitem->get('AccountCode')->getString(),
        $lineitem->get('TaxType')->getString(),
        $lineitem->get('TaxAmount')->getString(),
        $lineitem->get('LineAmount')->getString(),
      ];
    }

    $rows[] = ['', '', '', '', '', $this->t('Sub-Total'), $this->get('SubTotal')->getString()];
    $rows[] = ['', '', '', '', '', $this->t('Tax'), $this->get('TotalTax')->getString()];
    $rows[] = ['', '', '', '', '', $this->t('Total'), $this->get('Total')->getString()];

    $build['#items']['#rows'] = $rows;

    /** @var \Drupal\Core\TypedData\ListInterface $payments */
    $payments = $this->get('Payments');
    /** @var \Drupal\xero\Plugin\DataType\Payment $payment */
    foreach ($payments as $payment) {
      $build['payments']['#rows'][] = [
        $payment->get('PaymentType')->getString(),
        $payment->get('Status')->getString(),
        $payment->get('Amount')->getString(),
        $payment->get('Date')->getString(),
      ];
    }

    return $build;
  }

}
