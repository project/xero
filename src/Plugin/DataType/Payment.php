<?php

namespace Drupal\xero\Plugin\DataType;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\Attribute\DataType;
use Drupal\xero\TypedData\Definition\PaymentDefinition;

/**
 * Xero Payment type.
 *
 * @DataType(
 *   id = "xero_payment",
 *   label = @Translation("Xero Payment"),
 *   definition_class = "\Drupal\xero\TypedData\Definition\PaymentDefinition",
 *   list_class = "\Drupal\xero\Plugin\DataType\XeroItemList"
 * )
 */
#[DataType(
  id: 'xero_payment',
  label: new TranslatableMarkup('Xero Payment'),
  definition_class: PaymentDefinition::class,
  list_class: XeroItemList::class,
)]
class Payment extends XeroComplexItemBase {

  /**
   * {@inheritdoc}
   */
  static public $guid_name = 'PaymentID';

  /**
   * {@inheritdoc}
   */
  static public $xero_name = 'Payment';

  /**
   * {@inheritdoc}
   */
  static public $plural_name = 'Payments';

  /**
   * {@inheritdoc}
   */
  static public $label = 'Reference';

}
