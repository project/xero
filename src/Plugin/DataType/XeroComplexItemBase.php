<?php

namespace Drupal\xero\Plugin\DataType;

use Drupal\xero\TypedData\XeroComplexItemInterface;

/**
 * Xero base type for all complex xero types to inherit.
 *
 * These are data types with GUID and Plural properties.
 */
abstract class XeroComplexItemBase extends XeroItemBase implements XeroComplexItemInterface {

  /**
   * The primary identifier for the Xero type.
   *
   * @var string
   */
  static public $guid_name;

  /**
   * {@inheritdoc}
   */
  public static function getXeroProperty($name) {
    $allowed_props = ['guid_name', 'plural_name', 'xero_name', 'label'];
    if (!in_array($name, $allowed_props)) {
      throw new \InvalidArgumentException('Invalid xero property.');
    }

    return static::${$name};
  }

  /**
   * {@inheritdoc}
   */
  public function view(): array {
    $className = substr($this->getPluginId(), 5);

    return [
      '#theme' => $this->getPluginId(),
      '#item' => $this->getValue(),
      '#attributes' => [
        'class' => ['xero-item', 'xero-item--' . $className],
      ],
    ];
  }

}
