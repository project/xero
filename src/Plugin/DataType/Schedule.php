<?php

namespace Drupal\xero\Plugin\DataType;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\Attribute\DataType;
use Drupal\xero\TypedData\Definition\ScheduleDefinition;

/**
 * Describes a Xero schedule data type.
 *
 * @DataType(
 *   id = "xero_schedule",
 *   label = @Translation("Xero Schedule"),
 *   definition_class = "\Drupal\xero\TypedData\Definition\ScheduleDefinition",
 * )
 */
#[DataType(
  id: 'xero_schedule',
  label: new TranslatableMarkup('Xero Schedule'),
  definition_class: ScheduleDefinition::class,
  list_class: XeroItemList::class,
)]
class Schedule extends XeroItemBase {

  /**
   * {@inheritdoc}
   */
  static public $xero_name = 'Schedule';

  /**
   * {@inheritdoc}
   */
  static public $label = 'DueDate';

}
