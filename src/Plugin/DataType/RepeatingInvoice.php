<?php

namespace Drupal\xero\Plugin\DataType;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\Attribute\DataType;
use Drupal\xero\TypedData\Definition\RepeatingInvoiceDefinition;

/**
 * Describes a Xero repeating invoice.
 *
 * @DataType(
 *   id = "xero_repeating_invoice",
 *   label = @Translation("Xero Repeating Invoice"),
 *   definition_class = "\Drupal\xero\TypedData\Definition\RepeatingInvoiceDefinition",
 *   list_class = "\Drupal\xero\Plugin\DataType\XeroItemList"
 * )
 */
#[DataType(
  id: 'xero_repeating_invoice',
  label: new TranslatableMarkup('Xero Repeating Invoice'),
  definition_class: RepeatingInvoiceDefinition::class,
  list_class: XeroItemList::class,
)]
class RepeatingInvoice extends Invoice {

  /**
   * {@inheritdoc}
   */
  static public $guid_name = "RepeatingInvoiceID";

  /**
   * {@inheritdoc}
   */
  static public $xero_name = 'RepeatingInvoice';

  /**
   * {@inheritdoc}
   */
  static public $plural_name = 'RepeatingInvoices';

  /**
   * {@inheritdoc}
   */
  static public $label = 'RepeatingInvoiceID';

}
