<?php

namespace Drupal\xero\Plugin\DataType;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\Attribute\DataType;
use Drupal\xero\TypedData\Definition\ContactGroupDefinition;

/**
 * Describes a Xero contact group.
 *
 * @DataType(
 *   id = "xero_contact_group",
 *   label = @Translation("Xero Contact Group"),
 *   definition_class = "\Drupal\xero\TypedData\Definition\ContactGroupDefinition",
 *   list_class = "\Drupal\xero\Plugin\DataType\XeroItemList"
 * )
 */
#[DataType(
  id: 'xero_contact_group',
  label: new TranslatableMarkup('Xero Contact Group'),
  definition_class: ContactGroupDefinition::class,
  list_class: XeroItemList::class,
)]
class ContactGroup extends XeroComplexItemBase {

  /**
   * {@inheritdoc}
   */
  static public $guid_name = 'ContactGroupID';

  /**
   * {@inheritdoc}
   */
  static public $xero_name = 'ContactGroup';

  /**
   * {@inheritdoc}
   */
  static public $plural_name = 'ContactGroups';

  /**
   * {@inheritdoc}
   */
  static public $label = 'Name';

}
