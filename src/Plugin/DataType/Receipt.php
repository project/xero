<?php

namespace Drupal\xero\Plugin\DataType;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\Attribute\DataType;
use Drupal\xero\TypedData\Definition\ReceiptDefinition;

/**
 * Xero Receipt type.
 *
 * @DataType(
 *   id = "xero_receipt",
 *   label = @Translation("Xero Receipt"),
 *   definition_class = "\Drupal\xero\TypedData\Definition\ReceiptDefinition",
 *   list_class = "\Drupal\xero\Plugin\DataType\XeroItemList"
 * )
 */
#[DataType(
  id: 'xero_receipt',
  label: new TranslatableMarkup('Xero Receipt'),
  definition_class: ReceiptDefinition::class,
  list_class: XeroItemList::class,
)]
class Receipt extends XeroComplexItemBase {

  /**
   * {@inheritdoc}
   */
  static public $guid_name = 'ReceiptID';

  /**
   * {@inheritdoc}
   */
  static public $xero_name = 'Receipt';

  /**
   * {@inheritdoc}
   */
  static public $plural_name = 'Receipts';

  /**
   * {@inheritdoc}
   */
  static public $label = 'ReceiptNumber';

  /**
   * {@inheritdoc}
   */
  public function view(): array {
    $header = [
      $this->t('Description'),
      $this->t('Account code'),
      $this->t('Quantity'),
      $this->t('Unit amount'),
      $this->t('Tax type'),
      $this->t('Line amount'),
    ];
    $rows = [];

    $className = substr($this->getPluginId(), 5);

    $build = [
      '#theme' => $this->getPluginId(),
      '#receipt' => $this->getValue(),
      '#user' => $this->get('User')->view(),
      '#contact' => $this->get('Contact')->view(),
      '#items' => [
        '#theme' => 'table',
        '#header' => $header,
      ],
      '#attributes' => [
        'class' => ['xero-item', 'xero-item--' . $className],
      ],
    ];

    /** @var \Drupal\Core\TypedData\ListInterface $lineitems */
    $lineitems = $this->get('LineItems');
    /** @var \Drupal\xero\Plugin\DataType\LineItem $lineitem */
    foreach ($lineitems as $lineitem) {
      $rows[] = [
        $lineitem->get('Description')->getString(),
        $lineitem->get('AccountCode')->getString(),
        $lineitem->get('Quantity')->getString(),
        $lineitem->get('UnitAmount')->getString(),
        $lineitem->get('TaxType')->getString(),
        $lineitem->get('LineAmount')->getString(),
      ];
    }

    $build['#items']['#rows'] = $rows;

    return $build;
  }

}
