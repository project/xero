<?php

namespace Drupal\xero\Plugin\DataType;

use Drupal\Component\Utility\Html;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\Attribute\DataType;
use Drupal\xero\TypedData\Definition\AddressDefinition;

/**
 * Xero address type.
 *
 * @DataType(
 *   id = "xero_address",
 *   label = @Translation("Xero Address"),
 *   definition_class = "\Drupal\xero\TypedData\Definition\AddressDefinition",
 *   list_class = "\Drupal\xero\Plugin\DataType\XeroItemList"
 * )
 */
#[DataType(
  id: 'xero_address',
  label: new TranslatableMarkup('Xero Address'),
  definition_class: AddressDefinition::class,
  list_class: XeroItemList::class,
)]
class Address extends XeroItemBase {

  /**
   * {@inheritdoc}
   */
  static public $xero_name = 'Address';

  /**
   * {@inheritdoc}
   */
  static public $plural_name = 'Addresses';

  /**
   * {@inheritdoc}
   */
  static public $label = 'AddressLine1';

  /**
   * Gets one of the xero static properties by name.
   *
   * @param string $name
   *   The name of a static property on the class. This should be one of
   *   xero_name, plural_name or label_name.
   *
   * @return string|null
   *   The value of the property.
   */
  public static function getXeroProperty($name): ?string {
    if ($name === 'xero_name') {
      return self::$xero_name;
    }
    elseif ($name === 'plural_name') {
      return self::$plural_name;
    }
    elseif ($name === 'guid_name') {
      return '';
    }
    elseif ($name === 'label_name') {
      return 'AddressLine1';
    }

    throw new \InvalidArgumentException('Invalid xero property.');
  }

  /**
   * {@inheritdoc}
   */
  public function view(): array {
    $values = $this->getValue();
    $props = [
      'AttentionTo',
      'AddressLine1',
      'AddressLine2',
      'AddressLine3',
      'AddressLine4',
      'City',
      'Region',
      'PostalCode',
      'Country',
    ];

    $ret = [
      '#type' => 'container',
      '#prefix' => '<address>',
      '#suffix' => '</address>',
    ];

    foreach ($props as $prop) {
      if (isset($values[$prop])) {
        $suffix = !in_array($prop, ['City', 'Region']) ? '<br>' : '';
        $ret[$prop] = [
          '#markup' => Html::escape($values[$prop]) . $suffix,
        ];
      }
    }

    return $ret;
  }

}
