<?php

namespace Drupal\xero\Plugin\DataType;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\Attribute\DataType;
use Drupal\xero\TypedData\Definition\EmployeeDefinition;

/**
 * Xero Employee type.
 *
 * @DataType(
 *   id = "xero_employee",
 *   label = @Translation("Xero Employee"),
 *   definition_class = "\Drupal\xero\TypedData\Definition\EmployeeDefinition",
 *   list_class = "\Drupal\xero\Plugin\DataType\XeroItemList"
 * )
 */
#[DataType(
  id: 'xero_employee',
  label: new TranslatableMarkup('Xero Employee'),
  definition_class: EmployeeDefinition::class,
  list_class: XeroItemList::class,
)]
class Employee extends XeroComplexItemBase {

  /**
   * {@inheritdoc}
   */
  static public $guid_name = 'EmployeeID';

  /**
   * {@inheritdoc}
   */
  static public $xero_name = 'Employee';

  /**
   * {@inheritdoc}
   */
  static public $plural_name = 'Employees';

  /**
   * {@inheritdoc}
   */
  static public $label = 'LastName';

  /**
   * Is the employee active?
   *
   * @return bool
   *   TRUE if the employee is active.
   */
  public function isActive(): bool {
    return $this->get('Status')->getValue() === 'ACTIVE';
  }

}
