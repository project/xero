<?php

namespace Drupal\xero\Plugin\DataType;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\Attribute\DataType;
use Drupal\xero\TypedData\Definition\LineItemDefinition;

/**
 * Xero line item type.
 *
 * @DataType(
 *   id = "xero_line_item",
 *   label = @Translation("Xero Line Item"),
 *   definition_class = "\Drupal\xero\TypedData\Definition\LineItemDefinition",
 *   list_class = "\Drupal\xero\Plugin\DataType\XeroItemList"
 * )
 */
#[DataType(
  id: 'xero_line_item',
  label: new TranslatableMarkup('Xero Line Item'),
  definition_class: LineItemDefinition::class,
  list_class: XeroItemList::class,
)]
class LineItem extends XeroItemBase {

  /**
   * {@inheritdoc}
   */
  static public $xero_name = 'LineItem';

  /**
   * {@inheritdoc}
   */
  static public $plural_name = 'LineItems';

  /**
   * {@inheritdoc}
   */
  static public $label = 'Description';

  /**
   * {@inheritdoc}
   */
  public function view(): array {
    // Expectation is that line items are rendered by their parent.
    return [];
  }

}
