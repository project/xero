<?php

namespace Drupal\xero\Plugin\Validation\Constraint;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Validation\Attribute\Constraint as AttributeConstraint;
use Symfony\Component\Validator\Constraint;

/**
 * Provides the GUID validation from xero module drupal 7.
 */
#[AttributeConstraint(
  id: 'XeroGuidConstraint',
  label: new TranslatableMarkup('Guid Constraint'),
)]
class XeroGuidConstraint extends Constraint {

  /**
   * The message to display.
   *
   * @var string
   */
  public $message = 'This value should be a globally-unique identifier.';

}
