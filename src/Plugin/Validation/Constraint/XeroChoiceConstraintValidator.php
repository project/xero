<?php

namespace Drupal\xero\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraints\ChoiceValidator;

/**
 * Provides a generic choice constraint for Drupal.
 *
 * Class XeroChoiceConstraintValidator is an empty class that extends Symfony's
 * ChoiceValidator to make it discoverable.
 */
class XeroChoiceConstraintValidator extends ChoiceValidator {}
