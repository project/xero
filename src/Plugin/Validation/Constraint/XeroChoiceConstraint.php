<?php

namespace Drupal\xero\Plugin\Validation\Constraint;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Validation\Attribute\Constraint;
use Symfony\Component\Validator\Constraints\Choice;

/**
 * Extends Symfony Choice constraint.
 */
#[Constraint(
  id: 'XeroChoiceConstraint',
  label: new TranslatableMarkup('Xero Choice Constraint'),
)]
class XeroChoiceConstraint extends Choice {}
